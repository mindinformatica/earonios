//
//  Session+Convenience.swift
//  EarOn
//
//  Created by simonerocchi on 05/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import Foundation
import EarOnKit

extension Session : SessionTemplateProtocol {
    private var dateComponentsFormatter: DateComponentsFormatter {
        get {
            return AppDelegate.sharedInstance.defaultDateComponentsFormatter
        }
    }
    
    private var startDateFormatter: DateFormatter {
        get {
            let df = DateFormatter()
            df.dateStyle = .medium
            df.timeStyle = .short
            return df
        }
    }
    
    func add(measure: EOKMeasure) {
        if self.measures == nil {
            self.measures = [[String:Double]]()
        }
        self.measures?.append(measure.toDictionary())
    }
    
    var eokMeasures: [EOKMeasure]? {
        get {
            return self.measures?.map({ (m) -> EOKMeasure? in
                let md = m as! [String:Float]
                let ms = EOKMeasure(dictionary: md)
                return ms
            }) as? [EOKMeasure]
        }
    }
    
    var duration: String? {
        get {
            if let id = self.startDate as Date?, let fd = self.stopDate as Date? {
                return self.dateComponentsFormatter.string(from: id, to: fd)
            }
            return nil
        }
    }
    
    var formattedStartDate: String? {
        get {
            if let id = self.startDate as Date? {
                return self.startDateFormatter.string(from: id)
            }
            return nil
        }
    }
    
    var currentDuration: String? {
        if let id = self.startDate as Date? {
            return self.dateComponentsFormatter.string(from: id, to: Date())
        }
        return nil
    }
    
    var currentTime: Int {
        return -Int(self.startDate?.timeIntervalSinceNow ?? 0)
    }
    
    public var totalLength: Int {
        get {
            return self.steps?.reduce(0, +) ?? 0
        }
    }
    
    public var formattedTotalLength: String {
        return self.dateComponentsFormatter.string(from: TimeInterval(self.totalLength)) ?? ""
    }
    
    public var currentCountDown: Int {
        get {
            let ct = self.currentTime
            var pd: Int = 0
            if let steps = self.steps {
                for s in steps {
                    if ct >= pd && ct <= pd + s {
                        return pd + s - ct
                    } else {
                        pd += s
                    }
                    
                    
                }
            }
            return 0
        }
    }
    
    public var formatterdCurrentCountDown: String {
        return self.dateComponentsFormatter.string(from: TimeInterval(self.currentCountDown)) ?? ""
    }
    
    var advancement: Double? {
        get {
            let cd = Date().timeIntervalSince(self.startDate! as Date)
            let tl = TimeInterval(self.totalLength)
            return cd / tl
        }
    }
    
    public var name: String? {
        get {
            return self.templateName
        }
    }
    
    public func lineColorFor(time: TimeInterval!) -> UIColor! {
        print(time as Any)
        var si = 0
        var d:Double = 0
        if let steps = self.steps {
            for s in steps {
                d += Double(s)
                if time < d {
                    if si % 2 == 0 {
                        return StepsStackView.inflateColor
                    } else {
                        return StepsStackView.waitColor
                    }
                }
                si += 1
            }
        }
        return StepsStackView.inflateColor
    }
    
    public var maxVisibleY: Double {
        if let measures = self.eokMeasures, measures.count > 0 {
            let n = min(72,measures.count)
            let lasts = measures[measures.count - n...measures.count - 1]
            let lp = lasts.map({ (m) -> Float in
                return m.pressure
            })
            print("MAX lasts: \(lp)")
            let maxMeasure = lasts.max(by: { (a, b) -> Bool in
                return a.pressure < b.pressure
            })
            var maxPressure = max(maxMeasure?.pressure ?? 0,AppDelegate.sharedInstance.limiteSuperiore ?? maxMeasure?.pressure ?? 0)
            if maxPressure > 0 {
                maxPressure *= 1.2
            }
            return Double(maxPressure)
        }
//        if let mc = self.measures?.count, mc > 9 {
//            return Double(max(AppDelegate.sharedInstance.limiteSuperiore ?? 0,max(self.eokMeasures?.last?.pressure ?? 0,self.eokMeasures![mc - 10].pressure)))
//        }
        return Double(AppDelegate.sharedInstance.limiteSuperiore ?? 0)
    }
    
    public var minVisibleY: Double {
        if let measures = self.eokMeasures, measures.count > 0 {
            let n = min(72,measures.count)
            let lasts = measures[measures.count - n...measures.count - 1]
            
            let minMeasure = lasts.min(by: { (a, b) -> Bool in
                return a.pressure < b.pressure
            })
            let minPressure = min(minMeasure?.pressure ?? 0,AppDelegate.sharedInstance.limiteInferiore ?? minMeasure?.pressure ?? 0)
            return Double(minPressure)
        }
        //        if let mc = self.measures?.count, mc > 9 {
        //            return Double(max(AppDelegate.sharedInstance.limiteSuperiore ?? 0,max(self.eokMeasures?.last?.pressure ?? 0,self.eokMeasures![mc - 10].pressure)))
        //        }
        return Double(AppDelegate.sharedInstance.limiteInferiore ?? 0)
    }
    
    public var totalLaps: Int {
        return ((self.steps?.count ?? 0 % 2 == 0 ? self.steps?.count : ((self.steps?.count ?? 0) + 1)) ?? 0) / 2
    }
    
    public var currentLap: Int {
        return self.lapFor(time: self.currentTime)
    }
    
    public func lapFor(time:Int) -> Int {
        var td = 0
        var i = 0
        let cd = time
        if let steps = self.steps {
            for s in steps {
                if cd > td && cd <= td + s {
                    let l = i % 2 == 0 ? i : i - 1
                    return l / 2 + 1
                } else {
                    td += s
                    i += 1
                }
            }
        }
        return 0
    }
    
    public func toCSV() -> String {
        var csvString = ""
        if let measures = self.eokMeasures {
            for m in measures {
                csvString += "\(m.timestamp);\(m.temperature);\(m.pressure)\n"
            }
        }
        return csvString
    }
    
    public func toJSON() -> String {
        let json = "{\"data\":[\(self.eokMeasures?.map { (m) -> String in return "{\"pressure\":\(m.pressure),\"temperature\":\(m.temperature),\"timestamp\":\(m.timestamp),\"rawPressure\":\(m.rawPressure)}" }.joined(separator:",") ?? "")],\"mode\":\"\(self.mode ?? "")\",\"peripheralName\":\"\(self.peripheralName ?? "")\",\"startDate\":\(self.startDate!.timeIntervalSince1970),\"steps\":[\(self.steps!.map({ (step) -> String in return "\(step)" }).joined(separator:","))],\"stopDate\":\(self.stopDate!.timeIntervalSince1970),\"templateName\":\"\(self.templateName ?? "")\",\"descr\":\"\(self.descr ?? "")\"}"
        return json
        /*
         {
            data:"measures",
            mode:"",
            peripheralName:"",
            startDate:0,
            steps:[],
            stopDate:0,
            templateName:""
        */
    }
    
    public func fillFromJSON(json:String!) throws {
        if let data = json.data(using: .utf8, allowLossyConversion: false),
            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
            if let peripheralName = dict["peripheralName"] as? String, peripheralName.count > 0 {
                self.peripheralName = peripheralName
            }
            if let mode = dict["mode"] as? String, mode.count > 0 {
                self.mode = mode
            }
            if let startDate = dict["startDate"] as? TimeInterval {
                self.startDate = Date(timeIntervalSince1970: startDate)
            }
            if let stopDate = dict["stopDate"] as? TimeInterval {
                self.stopDate = Date(timeIntervalSince1970: stopDate)
            }
            if let steps = dict["steps"] as? [Int] {
                self.steps = steps
            }
            if let data = dict["data"] as? [[String:NSNumber]] {
                let measures = data.map { (m) -> EOKMeasure in
                    return EOKMeasure(dictionary: m)
                }
                for measure in measures {
                    self.add(measure: measure)
                }
            }
            if let descr = dict["descr"] as? String, descr.count > 0 {
                self.descr = descr
            }
        }
    }
}
