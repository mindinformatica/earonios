//
//  TutorialsViewController.swift
//  EarOn
//
//  Created by simonerocchi on 14/06/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import EarOnKit

class TutorialsViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.loadRequest(URLRequest(url: URL(string: NSLocalizedString("https://www.ubaproject.com/not-connected/", comment: "URL per tutorial senza connessione"))!))
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(TutorialsViewController.didConnectToPeripheral(_:)), name: Notification.Name.ConnectionEngine.DidConnectToPeripheral, object: nil)
        ConnectionEngine.shared.scan()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ConnectionEngine.shared.disconnect()
        ConnectionEngine.shared.stopScan()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didConnectToPeripheral(_ notification:Notification) {
        let urlString = NSLocalizedString("https://www.ubaproject.com/adfhlk65weyrjwhuy3brkwelrhajsdfkasjy3/en", comment: "URL per tutorial")
        self.webView.loadRequest(URLRequest(url: URL(string: urlString)!))
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
