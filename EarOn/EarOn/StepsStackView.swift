//
//  StepsStackView.swift
//  EarOn
//
//  Created by simonerocchi on 04/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit

protocol StepsStackViewDelegate {
    func selectedStepDidChange(selectedIndex: Int?, stepsStackView: StepsStackView)
    func stepValues(_ values: [Int], didChangeIn stepsStackView: StepsStackView)
}

class StepsStackView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    static var inflateColor: UIColor = UIColor(red: 21/255, green: 115/255, blue: 210/255, alpha: 1)
    static var waitColor: UIColor = UIColor.lightGray
    var delegate: StepsStackViewDelegate?
    var steps: [Int] = [] {
        didSet {
            self.showSteps()
        }
    }
    var stepsLength: Int {
        get {
            return self.steps.reduce(0, +)
        }
    }
    
    var stepsPercent: [CGFloat] {
        get {
            let tot = self.stepsLength
            return self.steps.map({ (s) -> CGFloat in
                return CGFloat(s) / CGFloat(tot)
            })
        }
    }
    
    var selectedStepIndex: Int? {
        willSet(newS) {
            if self.selectedStepIndex != nil {
                if let v = self.stepViewFor(index: self.selectedStepIndex!) {
                    self.show(stepView: v, selected: false)
                }
            }
        }
        didSet {
            if let ssi = self.selectedStepIndex {
                if let v = self.stepViewFor(index: ssi) {
                    self.show(stepView: v, selected: true)
                }
            }
            self.delegate?.selectedStepDidChange(selectedIndex: self.selectedStepIndex, stepsStackView: self)
        }
    }
    
    var selectedStepLength: Int? {
        get {
            if let ssi = self.selectedStepIndex {
                return self.steps[ssi]
            } else {
                return nil
            }
        }
        set(newV) {
            if let ssi = self.selectedStepIndex, let nv = newV {
                self.set(length: nv, forStepAt: ssi)
            }
        }
    }
    
    func stepViewFor(index: Int) -> UIView? {
        for v in self.subviews {
            if v.tag == index {
                return v
            }
        }
        return nil
    }
    
    func showSteps() {
        for v in self.subviews {
            v.removeFromSuperview()
        }
        
        var last: UIView?
        let stepsPercent = self.stepsPercent
        for index in 0...(self.steps.count - 1) {
            let newView = UIView() //UILabel()
            newView.tag = index
            newView.backgroundColor = self.colorFor(index: index)
//            newView.layer.borderWidth = 2
//            newView.layer.borderColor = self.colorFor(index: index).cgColor
//            newView.text = self.labelTextFor(index: index)
//            newView.textColor = UIColor.white
//            newView.textAlignment = .center
//            newView.minimumScaleFactor = 0.2
            newView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(StepsStackView.stepViewTapped(_:))))
            newView.isUserInteractionEnabled = true
            newView.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(newView)
            self.addConstraint(NSLayoutConstraint(item: newView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: newView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: stepsPercent[index], constant: 0))
            self.addConstraint(NSLayoutConstraint(item: newView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
            if last != nil {
                self.addConstraint(NSLayoutConstraint(item: newView, attribute: .leading, relatedBy: .equal, toItem: last, attribute: .trailing, multiplier: 1, constant: 0))
            } else {
                self.addConstraint(NSLayoutConstraint(item: newView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0))
            }
            last = newView
            if index == self.selectedStepIndex {
                self.show(stepView: newView, selected: true)
            }
        }
    }
    
//    private func labelTextFor(index: NSInteger) -> String {
//        return index % 2 == 0 ? NSLocalizedString("Equalize", comment: "Equalize") : NSLocalizedString("Relax", comment: "Relax")
//    }
    
    private func show(stepView:UIView, selected: Bool) {
        stepView.layer.borderColor = selected ? UIColor.blue.cgColor : nil
        stepView.layer.borderWidth = selected ? 2 : 0
    }
    
    @objc func stepViewTapped(_ sender: UITapGestureRecognizer) {
        self.selectedStepIndex = sender.view?.tag
    }
    
    func colorFor(index: Int) -> UIColor {
        return index % 2 == 0 ? StepsStackView.inflateColor : StepsStackView.waitColor
    }
    
    func removeLastStep() {
        _ = self.steps.popLast()
        self.showSteps()
        self.delegate?.stepValues(self.steps, didChangeIn: self)
    }
    
    func addStep(length: Int) {
        self.steps.append(length)
        self.showSteps()
        self.delegate?.stepValues(self.steps, didChangeIn: self)
    }
    
    func set(length:Int, forStepAt index:Int) {
        self.steps[index] = length
        self.showSteps()
        self.delegate?.stepValues(self.steps, didChangeIn: self)
    }
}
