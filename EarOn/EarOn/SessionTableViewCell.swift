//
//  SessionTableViewCell.swift
//  EarOn
//
//  Created by simonerocchi on 06/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit

class SessionTableViewCell: UITableViewCell {

    @IBOutlet weak var descrLabel: UILabel!
    @IBOutlet weak var modeImageView: UIImageView!
    @IBOutlet weak var modeLabel: UILabel!
    @IBOutlet weak var stepsStackView: StepsStackView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
