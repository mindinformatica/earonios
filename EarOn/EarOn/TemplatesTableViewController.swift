//
//  TemplatesTableViewController.swift
//  EarOn
//
//  Created by simonerocchi on 19/05/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import CoreData

class TemplatesTableViewController: UITableViewController,NSFetchedResultsControllerDelegate {
    lazy var fetchedResultController: NSFetchedResultsController<SessionTemplate> = {
        let moc = AppDelegate.sharedInstance.persistentContainer.viewContext
        let request: NSFetchRequest<SessionTemplate> = SessionTemplate.fetchRequest()
        let sd = NSSortDescriptor(key: "name", ascending: true)
        request.sortDescriptors = [sd]
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    override func viewDidLoad() {
        super.viewDidLoad()

        do {
            try self.fetchedResultController.performFetch()
        } catch {
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setToolbarHidden(true, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - FetchedResultControllerDelegate
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            self.tableView.reloadData()
        //self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .update:
            self.tableView.reloadData()
            //            if let cell = self.tableView.cellForRow(at: newIndexPath!) as? SessionTableViewCell {
            //                self.configure(cell: cell, session: anObject as! Session)
        //            }
        case .delete:
            self.tableView.deleteRows(at: [indexPath!], with: .automatic)
        default: break
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchedResultController.sections?[0].numberOfObjects ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TEMPLATE_CELL", for: indexPath) as! TemplateTableViewCell

        let template = self.fetchedResultController.object(at: indexPath)
        
        cell.template = template
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "templateSegue", let nc = segue.destination as? UINavigationController,let dc = nc.topViewController as? TemplateViewController, let cell = sender as? UITableViewCell, let indexPath = self.tableView.indexPath(for: cell) {
            let template = self.fetchedResultController.object(at: indexPath)
            dc.sessionTemplate = template
        } else if segue.identifier == "templateDidChangeSegue", let cell = sender as? TemplateTableViewCell, var dc = segue.destination as? SettingViewController {
            dc.sessionTemplate = cell.template
        }
    }

}
