//
//  GameStartViewController.swift
//  EarOn
//
//  Created by simonerocchi on 26/09/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import EarOnKit

class GameStartViewController: UIViewController {

    @IBOutlet weak var beginLabel: UILabel!
    @IBOutlet weak var bestScoreLabel: UILabel!
    var ready = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(GameStartViewController.didConnectToPeripheral(_:)), name: Notification.Name.ConnectionEngine.DidConnectToPeripheral, object: nil)
        if let bs = UserDefaults.standard.object(forKey: bestScoreKey) as? NSNumber {
            self.bestScoreLabel.text = "\(bs)"
        }
        self.beginLabel.isUserInteractionEnabled = false
        if AppDelegate.sharedInstance.sogliaCompensazione == nil {
            self.beginLabel.text = NSLocalizedString("touch to set the equalization threshold", comment: "touch to set the equalization threshold")
        } else {
            self.beginLabel.text = NSLocalizedString("touch to connect a device", comment: "touch to connect a device")
        }
        ConnectionEngine.shared.scan()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ConnectionEngine.shared.disconnect()
        ConnectionEngine.shared.stopScan()
    }
    
    @objc func didConnectToPeripheral(_ notification: NSNotification) {
        if AppDelegate.sharedInstance.sogliaCompensazione == nil {
            self.beginLabel.text = NSLocalizedString("touch to set the equalization threshold", comment: "touch to set the equalization threshold")
        } else {
            self.beginLabel.isUserInteractionEnabled = false
            self.beginLabel.text = NSLocalizedString("touch to begin", comment: "touch to begin")
            self.ready = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapped(_ sender: Any) {
        if self.ready {
            self.performSegue(withIdentifier: "gameSegue", sender: nil)
        } else {
            self.tabBarController?.selectedIndex = 1
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
