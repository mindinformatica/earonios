//
//  GameScene.swift
//  EarOn
//
//  Created by simonerocchi on 18/06/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import SpriteKit

let BallCategoryName = "ball"
let PaddleCategoryName = "paddle"
let BlockCategoryName = "block"
let GameMessageName = "gameMessage"

class GameScene: SKScene {
    var ball: SKSpriteNode {
        get {
            return self.childNode(withName: BallCategoryName) as! SKSpriteNode
        }
    }
    
    var background: SKSpriteNode {
        get {
            return self.childNode(withName: "bg") as! SKSpriteNode
        }
    }
    
    var fish: SKSpriteNode {
        get {
            return self.childNode(withName: "fish") as! SKSpriteNode
        }
    }
    
    var fish2: SKSpriteNode {
        get {
            return self.childNode(withName: "fish2") as! SKSpriteNode
        }
    }
    
    var pavimento: SKSpriteNode {
        get {
            return self.childNode(withName: "pavimento") as! SKSpriteNode
        }
    }
    
    var tetto: SKSpriteNode {
        get {
            return self.childNode(withName: "tetto") as! SKSpriteNode
        }
    }
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        print("MOVE")
        // 1
        //let borderBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        // 2
        //borderBody.friction = 0
        // 3
        //self.physicsBody = borderBody
        //self.physicsBody?.contactTestBitMask = 0
    }
}
