import UIKit
import SpriteKit
import EarOnKit

let bestScoreKey = "bestScoreKey"

class GameViewController: UIViewController,SKPhysicsContactDelegate {
    var scene: GameScene? {
        didSet {
            self.scene?.physicsWorld.contactDelegate = self
        }
    }
    let palette : [Float : UIColor] = [0.0 : UIColor.black, 1.0 : UIColor.green]//[0.0 : UIColor.green,0.5 : UIColor.yellow,1.0 : UIColor.red]
    var minTime: UInt32 {
        get {
            return self.counter > 240 ? 0 : 2
        }
    }
    var maxTime: UInt32 {
        get {
            return self.counter > 240 ? 2 : 4
        }
    }
    var nanColor: UIColor {
        get {
            return self.palette[1.0]!
        }
    }
    @IBOutlet weak var fLabel: UILabel!
    @IBOutlet weak var pressLabel: UILabel!
    @IBOutlet weak var massLabel: UILabel!
    @IBOutlet weak var cfLabel: UILabel!
    @IBOutlet weak var skView: SKView!
    @IBOutlet weak var x2Button: UIButton!
    
    @IBOutlet weak var coverViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var stopButton: UIBarButtonItem!
    var haveToPresent2: Bool = false {
        didSet {
            let i = UIImage(named: "pesciolinocieco\(self.haveToPresent2 ? "1" : "")")
            self.x2Button.setImage(i, for: .normal)
            if self.haveToPresent2 {
                self.presentFish(true)
            }
        }
    }
    lazy var gellyfishTextures: [SKTexture] = {
        var tt = [SKTexture]()
        for i in 0...49 {
            tt.append(SKTexture(imageNamed: "medusa\(i)"))
        }
        return tt
    }()
    
    lazy var deadGellyfishTextures: [SKTexture] = {
        var tt = [SKTexture]()
        for i in 0...49 {
            tt.append(SKTexture(imageNamed: "medusa_morta\(i)"))
        }
        return tt
    }()
    
    lazy var backgroundTextures: [SKTexture] = {
        var tt = [SKTexture]()
        for i in 0...74 {
            tt.append(SKTexture(imageNamed: "sfondo\(i)"))
        }
        return tt
    }()
    
    lazy var fishTextures: [SKTexture] = {
        var tt = [SKTexture]()
        for i in 1...5 {
            tt.append(SKTexture(imageNamed: "pesciolinoscelto_\(i)"))
        }
        return tt
    }()
    
    lazy var reverseFishTextures : [SKTexture] = {
        var tt = [SKTexture]()
        for i in 1...5 {
            tt.append(SKTexture(imageNamed: "pesciolinosceltoriflesso_\(i)"))
        }
        return tt
    }()
    
    lazy var filledGellyfishTexture: SKTexture = SKTexture(imageNamed: "medusapiena")
    lazy var animateGellyfishAction: SKAction = SKAction.repeatForever(SKAction.animate(with: self.gellyfishTextures, timePerFrame: 1 / 24))
    lazy var deadGellyfishAction: SKAction = SKAction.animate(with: self.deadGellyfishTextures, timePerFrame: 1 / 16)
    var score: Int = 0 {
        didSet {
            self.fLabel.text = "\(self.score)"
        }
    }
    var counter: Int = 0
    var minDelta: Float = 0
    var startDate: Date? /*{
        didSet {
            self.started = true
        }
    }*/
    var started: Bool = false
    var gameover: Bool = false
    var scoring: Bool = false
    var bestScore: NSNumber? {
        get {
            return UserDefaults.standard.object(forKey: bestScoreKey) as? NSNumber
        }
        set(nbs) {
            UserDefaults.standard.setValue(nbs, forKey: bestScoreKey)
            UserDefaults.standard.synchronize()
            self.massLabel.text = "\(nbs?.intValue ?? 0)"
        }
    }
    var fishTimer: Timer?
    var fish2Timer: Timer?
    lazy var fish: SKSpriteNode = self.scene!.fish
    lazy var fish2: SKSpriteNode = self.scene!.fish2
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        self.massLabel.text = "\(self.bestScore?.intValue ?? 0)"
        self.skView.backgroundColor = SKColor.white
//        if let scene = GameScene(fileNamed:"GameScene") {
//            // Configure the view.
//            self.skView.showsFPS = true
//            self.skView.showsNodeCount = true
//            self.skView.allowsTransparency = true
//            /* Sprite Kit applies additional optimizations to improve rendering performance */
//            self.skView.ignoresSiblingOrder = true
//
//            /* Set the scale mode to scale to fit the window */
//            //scene.scaleMode = .aspectFit
//            self.scene = scene
//
//        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if self.skView.scene == nil {
            let sceneFile = UIDevice.current.userInterfaceIdiom == .phone ? "GameScene" : "IPadGameScene"
            if let scene = GameScene(fileNamed: sceneFile) {
                // Configure the view.
                self.skView.allowsTransparency = true
                /* Sprite Kit applies additional optimizations to improve rendering performance */
                self.skView.ignoresSiblingOrder = true
                
                /* Set the scale mode to scale to fit the window */
                self.scene = scene
                self.scene?.size = self.skView.frame.size
                self.scene?.background.size = self.skView.frame.size
                self.scene?.pavimento.position = CGPoint(x: self.scene!.pavimento.position.x, y: -self.skView.frame.size.height / 2)
                self.scene?.tetto.position = CGPoint(x: self.scene!.pavimento.position.x, y: self.skView.frame.size.height / 2)
                self.scene?.scaleMode = .fill
                self.skView.presentScene(scene)
                
            }
        }
//        let c = (self.view.frame.size.width - (self.scene?.size.width)!) / 2
//        self.coverViewWidthConstraint.constant = c
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.didReceiveMeasureUpdate(_:)), name: Notification.Name.ConnectionEngine.DidReceiveMeasureUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.didConnectToPeripheral(_:)), name: Notification.Name.ConnectionEngine.DidConnectToPeripheral, object: nil)
        if let ball = self.scene?.ball {
            ball.physicsBody?.mass = AppDelegate.sharedInstance.ballMass!
        }
        self.startDate = Date()
        ConnectionEngine.shared.scan()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ConnectionEngine.shared.disconnect()
        ConnectionEngine.shared.stopScan()
        self.fishTimer?.invalidate()
        self.fish2Timer?.invalidate()
    }


    override var shouldAutorotate : Bool {
        return false
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden : Bool {
        return true
    }
    func timer(due: Bool?) -> Timer? {
        return due ?? false ? self.fish2Timer : self.fishTimer
    }
    
    func setTimer(_ timer: Timer, due: Bool?) {
        if due ?? false {
            self.fish2Timer = timer
        } else {
            self.fishTimer = timer
        }
    }
    @objc func didConnectToPeripheral(_ notification: NSNotification) {
        if let gelly = self.scene?.ball {
            gelly.run(self.animateGellyfishAction)
        }
        if let background = self.scene?.background {
            background.run(SKAction.repeatForever(SKAction.animate(with: self.backgroundTextures, timePerFrame: 1 / 24)))
        }
        UIView.animate(withDuration: 0.5) {
            self.fLabel.alpha = 1
            self.cfLabel.alpha = 1
            self.massLabel.alpha = 1
            self.pressLabel.alpha = 1
        }
    }
    
    @objc func didReceiveMeasureUpdate(_ notification: NSNotification) {
        if let measure = notification.userInfo?[ConnectionEngineUserInfoKeys.EOKMeasure] as? EOKMeasure, let ball = self.scene?.ball, !self.gameover {
            let surface: CGFloat = 402.0
            let bcf = AppDelegate.sharedInstance.ballConversionFactor!
            let force = (CGFloat(measure.pressure) / 101325 * surface) * bcf
            if let k = AppDelegate.sharedInstance.sogliaCompensazione {
                let x = measure.pressure
                var cbf: CGFloat
                if x <= k {
                    cbf = CGFloat(pow(x / k,2))
                } else {
                    cbf = CGFloat(pow((x - 2 * k) / k,2))
                }
                cbf = min(max(cbf,0),1)
                ball.colorBlendFactor = cbf
                let c = self.colorForWeight(weight: Float(cbf))
                self.pressLabel.textColor = c
                //se è la prima misura in un secondo
                if self.counter % 8 == 0 && self.scoring {
                    //calcolo il punteggio del secondo precedente
                    self.score += self.calcolaPunteggioUltimoSecondo()
                    if self.score > self.bestScore?.intValue ?? 0 {
                        self.bestScore = NSNumber(value: self.score)
                    }
                    //azzero il delta minimo registrato
                    self.minDelta = 0
                }
                //registro il delta come minimo se è minore del delta minimo del secondo
                self.minDelta = max(self.minDelta,abs(k - measure.pressure))
            }
            if measure.pressure > 0 {
                ball.physicsBody?.applyImpulse(CGVector(dx: 0, dy: force))
            }
            if measure.pressure > 3 {
                self.started = true
                self.scoring = true
            }
            if self.counter % 2 == 0 {
                self.pressLabel.text = String(format: "%.1f hPa",measure.pressure)
                
                
            }
            if self.counter % 8 == 0 {
                let i = Date().timeIntervalSince(self.startDate!)
                let fi = AppDelegate.sharedInstance.defaultDateComponentsFormatter.string(from: i)
                self.cfLabel.text = fi
            }
            
            if self.counter == 24 {
                self.startFishTimer(false)
            }
            
//            if self.counter == 60 {
//                UIView.animate(withDuration: 1, animations: {
//                    self.x2Button.alpha = 1
//                })
//            }
            
            self.counter += 1
            
            
        }
    }
    func calcolaPunteggioUltimoSecondo() -> Int {
        if self.counter == 0 {
            return 0
        } else if self.minDelta <= 2 {
            return 50
        } else if self.minDelta <= 3 {
            return 30
        } else if self.minDelta <= 5 {
            return 18
        } else if self.minDelta <= 10 {
            return 13
        } else if self.minDelta <= 15 {
            return 5
        } else {
            return 0
        }
    }
    
    @IBAction func stopAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func colorForWeight(weight: Float) -> UIColor {
        if weight.isNaN {
            return self.nanColor
        }
        let p : Float = weight
        var keys : [Float] = Array(self.palette.keys) as [Float]
        if keys.contains(p) {
            return self.palette[p]!
        }
        keys.sort(by: <)
        var i : Int = 1
        for k in 0...keys.count - 1 {
            if p < keys[k] {
                i = k
                break;
            }
        }
        let upperKey : Float = keys[i]
        let lowerKey : Float = keys[i - 1]
        let upper : UIColor = self.palette[upperKey]!
        let lower : UIColor = self.palette[lowerKey]!
        let range : Float = upperKey - lowerKey
        let rangePct = (p - lowerKey) / range
        let pctLower = 1 - rangePct
        let pctUpper = rangePct
        var lr : CGFloat = 0
        var lg : CGFloat = 0
        var lb : CGFloat = 0
        var la : CGFloat = 0
        var ur : CGFloat = 0
        var ug : CGFloat = 0
        var ub : CGFloat = 0
        var ua : CGFloat = 0
        lower.getRed(&lr, green: &lg, blue: &lb, alpha: &la)
        upper.getRed(&ur, green: &ug, blue: &ub, alpha: &ua)
        let r : CGFloat = lr * CGFloat(pctLower) + ur * CGFloat(pctUpper)
        let g : CGFloat = lg * CGFloat(pctLower) + ug * CGFloat(pctUpper)
        let b : CGFloat = lb * CGFloat(pctLower) + ub * CGFloat(pctUpper)
        let c : UIColor = UIColor(red: r, green: g, blue: b, alpha: 1)
        return c
    }
    func didBegin(_ contact: SKPhysicsContact) {
        if let gelly = self.scene?.ball, gelly.action(forKey: "gameoverAction") == nil && (contact.bodyA.node == self.scene?.ball || contact.bodyB.node == self.scene?.ball) && !self.gameover {
            if self.started && self.counter > 24 && (contact.bodyA.node == self.scene?.pavimento || contact.bodyB.node == self.scene?.pavimento) && !self.gameover {
                self.scoring = false
                gelly.removeAllActions()
                gelly.run(SKAction.sequence([self.deadGellyfishAction,SKAction.run({
                    self.gameOver()
                })]), withKey: "gameoverAction")
            } else if contact.bodyA.node != self.scene?.pavimento && contact.bodyB.node != self.scene?.pavimento {
                self.gameOver()
            }
        }
    }
    
    func didEnd(_ contact: SKPhysicsContact) {
        if contact.bodyA.node == self.scene?.ball || contact.bodyB.node == self.scene?.ball {
            self.scene?.ball.removeAction(forKey: "gameoverAction")
            self.scene?.ball.run(self.animateGellyfishAction)
            if (contact.bodyA.node?.name == "pavimento" || contact.bodyB.node?.name == "pavimento" && !self.gameover) && self.started {
                self.scoring = true
            }
        }
    }
    
    func gameOver() {
        if let ball = self.scene?.ball {
            self.gameover = true
            ball.removeAllActions()
            ball.physicsBody?.affectedByGravity = false
            ball.physicsBody?.pinned = true
            ball.run(SKAction.sequence([
                SKAction.scale(to: 3, duration: 0.03),
                SKAction.wait(forDuration: 0.05)]),completion: {
                    ball.physicsBody?.pinned = false
                    ball.physicsBody?.affectedByGravity = true
                    ball.run(SKAction.rotate(byAngle: CGFloat.pi, duration: 0.5), completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
            })
        }
    }
    
    func startFishTimer(_ due: Bool?) {
        let time = arc4random_uniform(maxTime - minTime + 1) + minTime
        self.setTimer(Timer.scheduledTimer(withTimeInterval: TimeInterval(time), repeats: false) { (_) in
            if !self.gameover {
                self.presentFish(due)
            }
        }, due: due)
    }
    
    func presentFish(_ due: Bool?) {
        print("PRESENT FISH \(due ?? false ? "due" : "uno")")
         self.timer(due: due)?.invalidate()
        let fish =  !(due ?? false) ? self.fish : self.fish2
        
        //            fish.physicsBody = SKPhysicsBody(rectangleOf: fish.size)
        //            fish.physicsBody?.allowsRotation = true
        //            fish.physicsBody?.affectedByGravity = false
        //            fish.physicsBody?.contactTestBitMask = 1
        //            self.scene?.addChild(fish)
        fish.removeAllActions()
        fish.setScale(1)
        let r = Double(arc4random_uniform(3))
        let scaleFactor = CGFloat(r / 2 + 1)
        fish.setScale(scaleFactor)
        let verso = arc4random_uniform(2)
        fish.run(SKAction.repeatForever(SKAction.animate(with: verso == 1 ? self.fishTextures : self.reverseFishTextures, timePerFrame: 0.1)))
        let segno = arc4random_uniform(2)
        let y = CGFloat(Int(arc4random_uniform(121) + 150) * (segno == 0 ? -1 : 1))
        let sceneHalfWidth = self.scene!.size.width / 2
        let fishHalfWidth = fish.size.width / 2
        let x0 = /*300*/ (sceneHalfWidth + fishHalfWidth) * (verso == 0 ? -1 : 1)
        let x1 = CGFloat(/*300.0*/ (sceneHalfWidth + fishHalfWidth) * (verso == 0 ? 1 : -1))
        fish.position = CGPoint(x: x0, y: y)
        //fish.zPosition = 8
        //            fish.run(SKAction.repeatForever(SKAction.animate(with: self.fishTextures, timePerFrame: 0.3)))
        fish.run(SKAction.sequence([SKAction.scale(to: scaleFactor, duration: 0.1),SKAction.moveTo(x: x1 /*self.scene!.frame.size.width + fish.frame.size.width / 2*/, duration: 5) ]) , completion: {
            //                fish.removeFromParent()
            fish.position = CGPoint(x: x0, y: y)
            if !(due ?? false) || self.haveToPresent2 {
                self.startFishTimer(due)
            }
        })
        //SKSpriteNode(texture: self.fishTextures[0], size: CGSize(width: 120/*129*/, height:60 /*60*/))//SKSpriteNode(color: UIColor.red, size: CGSize(width: 100, height: 100)) //SKSpriteNode(imageNamed: "framepesce1")//SKSpriteNode(texture: self.fishTextures[0], size: CGSize(width: 200, height: 80))
        
        
        
    }
    
    @IBAction func x2Action(_ sender: Any) {
        self.haveToPresent2 = !self.haveToPresent2
    }
}
