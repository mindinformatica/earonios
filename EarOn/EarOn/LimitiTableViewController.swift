//
//  LimitiTableViewController.swift
//  EarOn
//
//  Created by simonerocchi on 04/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit

class LimitiTableViewController: UITableViewController {

    @IBOutlet weak var compensazioneTextField: UITextField!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var superioreTextField: UITextField!
    @IBOutlet weak var inferioreTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let li = AppDelegate.sharedInstance.limiteInferiore
        let ls = AppDelegate.sharedInstance.limiteSuperiore
        let sc = AppDelegate.sharedInstance.sogliaCompensazione
        self.inferioreTextField.text = li == nil ? "" : String(format: "%.1f",li!)
        self.superioreTextField.text = ls == nil ? "" : String(format: "%.1f",ls!)
        self.compensazioneTextField.text = sc == nil ? "" : String(format: "%.1f",sc!)
    }
    @IBAction func textFieldEditingChanged(_ sender: Any) {
        if sender as? UITextField == self.inferioreTextField {
            AppDelegate.sharedInstance.limiteInferiore = Float(self.inferioreTextField.text ?? "0")
        } else if sender as? UITextField == self.superioreTextField {
            AppDelegate.sharedInstance.limiteSuperiore = Float(self.superioreTextField.text ?? "0")
        } else if sender as? UITextField == self.compensazioneTextField {
            AppDelegate.sharedInstance.sogliaCompensazione = Float(self.compensazioneTextField.text ?? "0")
        }
    }

    @IBAction func doneAction(_ sender: Any) {
        AppDelegate.sharedInstance.limiteInferiore = Float(self.inferioreTextField.text!)
        AppDelegate.sharedInstance.limiteSuperiore = Float(self.superioreTextField.text!)
        AppDelegate.sharedInstance.sogliaCompensazione = Float(self.compensazioneTextField.text!)
        self.navigationController?.popViewController(animated: true)
    }
    
}
