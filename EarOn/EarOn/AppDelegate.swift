//
//  AppDelegate.swift
//  EarOn
//
//  Created by simonerocchi on 31/03/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import CoreData
import EarOnKit
import CoreBluetooth

let defaultPeripheralNameKey = "defaultPeripheralNameKey"
let limiteInferioreKey = "limiteInferioreKey"
let sogliaCompensazioneKey = "sogliaCompensazione"
let valoreZeroKey = "valoreZeroKey"
let limiteSuperioreKey = "limiteSuperioreKey"
let ballMassKey = "ballMassKey"
let defaultBallMassKey = "defaultBallMassKey"
let ballConversionFactorKey = "ballConversionFactorKey"
let defaultBallConversionFactorKey = "defaultBallConversionFactorKey"
let modeKey = "modeKey"
let templateKey = "templateKey"

let secondaryColor = UIColor(red: 41 / 255, green: 60 / 255, blue: 180 / 255, alpha: 1)

extension UIViewController {
    @objc func presentRecordigViewController(_ sender: Any?) {
        let rc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "recordingVC")
        self.present(rc, animated: true, completion: nil)
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UINavigationControllerDelegate {

    static var sharedInstance: AppDelegate {
        get {
            return UIApplication.shared.delegate as! AppDelegate
        }
    }
    
    var window: UIWindow?
    var template: SessionTemplate? {
        get {
            let req: NSFetchRequest = SessionTemplate.fetchRequest()
            req.predicate = NSPredicate(format: "lastUseDate <> nil")
            let sd = NSSortDescriptor(key: "lastUseDate", ascending: false)
            req.sortDescriptors = [sd]
            do {
                let ts = try self.persistentContainer.viewContext.fetch(req)
                if let t = ts.first {
                    return t
                } else {
                    req.predicate = NSPredicate(format: "name = %@", "default")
                    let ts = try self.persistentContainer.viewContext.fetch(req)
                    if let t = ts.first {
                        return t
                    }
                }
            } catch let error {
                print(error)
            }
           return nil
        }
        set(nt) {
            nt?.lastUseDate = Date() as NSDate
            do {
                try nt?.managedObjectContext?.save()
            } catch let error {
                print(error)
            }
        }
    }
    var mode: String? {
        get {
            return UserDefaults.standard.string(forKey: modeKey)
        }
        set(nm) {
            UserDefaults.standard.set(nm, forKey: modeKey)
            UserDefaults.standard.synchronize()
        }
    }
    var defaultPeripheralName: String? {
        get {
            return UserDefaults.standard.string(forKey: defaultPeripheralNameKey)
        }
        set(newP) {
            UserDefaults.standard.set(newP, forKey: defaultPeripheralNameKey)
            if newP != self.defaultPeripheralName {
                self.valoreZero = nil
            }
            UserDefaults.standard.synchronize()
            ConnectionEngine.shared.peripheralDefaultName = newP
        }
    }
    
    var limiteInferiore: Float? {
        get {
            let li = UserDefaults.standard.float(forKey: limiteInferioreKey)
            return li > 0 ? li : nil
        }
        set(li) {
            UserDefaults.standard.set(li, forKey: limiteInferioreKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    var limiteSuperiore: Float? {
        get {
            let int = UserDefaults.standard.float(forKey: limiteSuperioreKey)
            return int > 0 ? int : nil
        }
        set(li) {
            UserDefaults.standard.set(li, forKey: limiteSuperioreKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    var sogliaCompensazione: Float? {
        get {
            let int = UserDefaults.standard.float(forKey: sogliaCompensazioneKey)
            return int > 0 ? int : nil
        }
        set(li) {
            UserDefaults.standard.set(li, forKey: sogliaCompensazioneKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    var valoreZero: Float? {
        get {
            let li = UserDefaults.standard.float(forKey: valoreZeroKey)
            return li
        }
        set(li) {
            ConnectionEngine.zero = li ?? 0
            UserDefaults.standard.set(li, forKey: valoreZeroKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    var ballMass: CGFloat? {
        get {
            if let bm = UserDefaults.standard.object(forKey: ballMassKey) as? CGFloat {
                return bm
            } else if let bm = UserDefaults.standard.object(forKey: defaultBallMassKey) as? CGFloat {
                return bm
            } else {
                UserDefaults.standard.set(0.00893608666956425, forKey: defaultBallMassKey)
                UserDefaults.standard.synchronize()
                return UserDefaults.standard.object(forKey: defaultBallMassKey) as? CGFloat ?? 0
            }
        }
        set(bm) {
            UserDefaults.standard.set(bm, forKey: ballMassKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    private func parametriPerBCF(x:CGFloat?) -> (x0:CGFloat,y0:CGFloat,x1:CGFloat,y1:CGFloat)? {
        if let sc = x == 29 ? 27 : x {
            
            if sc < 6.5 {
                return (x0:CGFloat(5),y0:CGFloat(75),x1:CGFloat(6.5),y1:CGFloat(70))
            } else if sc < 7.0 {
                return (x0:CGFloat(6.5),y0:CGFloat(70),x1:CGFloat(7),y1:CGFloat(60))
            } else if sc < 7.5 {
                return (x0:CGFloat(7),y0:CGFloat(60),x1:CGFloat(7.5),y1:CGFloat(55))
            } else if sc < 8 {
                return (x0:CGFloat(7.5),y0:CGFloat(55),x1:CGFloat(8),y1:CGFloat(50))
            } else if sc < 9 {
                return (x0:CGFloat(8),y0:CGFloat(50),x1:CGFloat(9),y1:CGFloat(45))
            } else if sc < 10 {
                return (x0:CGFloat(9),y0:CGFloat(45),x1:CGFloat(10),y1:CGFloat(40))
            } else if sc < 11 {
                return (x0:CGFloat(10),y0:CGFloat(40),x1:CGFloat(11),y1:CGFloat(37))
            } else if sc < 11.5 {
                return (x0:CGFloat(11),y0:CGFloat(37),x1:CGFloat(11.5),y1:CGFloat(35))
            } else if sc < 12 {
                return (x0:CGFloat(11.5),y0:CGFloat(35),x1:CGFloat(12),y1:CGFloat(32))
            } else if sc < 13.5 {
                return (x0:CGFloat(12),y0:CGFloat(32),x1:CGFloat(13.5),y1:CGFloat(29.5))
            } else if sc < 15 {
                return (x0:CGFloat(13.5),y0:CGFloat(29.5),x1:CGFloat(15),y1:CGFloat(27))
            } else if sc < 18 {
                return (x0:CGFloat(15),y0:CGFloat(27),x1:CGFloat(18),y1:CGFloat(23.5))
            } else if sc < 19 {
                return (x0:CGFloat(18),y0:CGFloat(23.5),x1:CGFloat(19),y1:CGFloat(21.8))
            } else if sc < 20 {
                return (x0:CGFloat(19),y0:CGFloat(21.8),x1:CGFloat(20),y1:CGFloat(20))
            } else if sc < 22.5 {
                return (x0:CGFloat(20),y0:CGFloat(20),x1:CGFloat(22.5),y1:CGFloat(18))
            } else if sc < 25 {
                return (x0:CGFloat(22.5),y0:CGFloat(18),x1:CGFloat(25),y1:CGFloat(16))
            } else if sc < 27 {
                return (x0:CGFloat(25),y0:CGFloat(16),x1:CGFloat(27),y1:CGFloat(15))
            } else if sc < 30 {
                return (x0:CGFloat(27),y0:CGFloat(15),x1:CGFloat(30),y1:CGFloat(14))
            } else if sc < 32 {
                return (x0:CGFloat(30),y0:CGFloat(14),x1:CGFloat(32),y1:CGFloat(13))
            } else if sc < 35 {
                return (x0:CGFloat(32),y0:CGFloat(13),x1:CGFloat(35),y1:CGFloat(12))
            } else if sc < 40 {
                return (x0:CGFloat(35),y0:CGFloat(12),x1:CGFloat(40),y1:CGFloat(10))
            } else if sc < 45 {
                return (x0:CGFloat(40),y0:CGFloat(10),x1:CGFloat(45),y1:CGFloat(9))
            } else if sc < 50 {
                return (x0:CGFloat(45),y0:CGFloat(9),x1:CGFloat(50),y1:CGFloat(8))
            } else if sc < 60 {
                return (x0:CGFloat(50),y0:CGFloat(8),x1:CGFloat(60),y1:CGFloat(7))
            } else {
                return (x0:CGFloat(60),y0:CGFloat(7),x1:CGFloat(70),y1:CGFloat(6))
                
            }
        } else {
            return nil
        }
    }
    
    func calcolaBCF(x:CGFloat) -> CGFloat? {
        if let p = self.parametriPerBCF(x: x) {
            let bcf = (p.x0 - p.x1) / (p.y0 - p.y1) * (CGFloat(x) - p.x0) + p.y0
            return bcf
        } else {
            return nil
        }
    }
    
    var ballConversionFactor: CGFloat? {
        get {
            return self.sogliaCompensazione == nil ? nil : self.calcolaBCF(x: CGFloat(self.sogliaCompensazione ?? 25))
            
//            if let bm = UserDefaults.standard.object(forKey: ballConversionFactorKey) as? CGFloat {
//                return bm
//            } else if let bm = UserDefaults.standard.object(forKey: defaultBallConversionFactorKey) as? CGFloat {
//                return bm
//            } else {
//                UserDefaults.standard.set(50, forKey: defaultBallConversionFactorKey)
//                UserDefaults.standard.synchronize()
//                return UserDefaults.standard.object(forKey: defaultBallConversionFactorKey) as? CGFloat ?? 0
//            }
            
        }
    }
    
    lazy var defaultDateComponentsFormatter: DateComponentsFormatter = {
        let dcf = DateComponentsFormatter()
        dcf.zeroFormattingBehavior = .dropLeading
        dcf.maximumUnitCount = 0
        dcf.unitsStyle = .positional //.brief //.abbreviated
        return dcf
    }()
    
    func createDefaultTemplateIfNeeded() {
        do {
            let templates = try self.persistentContainer.viewContext.fetch(SessionTemplate.fetchRequest())
            if templates.count == 0 {
                let dt = SessionTemplate(context: self.persistentContainer.viewContext)
                dt.name = "default"
                dt.steps = [120]
                self.saveContext()
                self.template  = dt
                self.mode = "free"
            }
        } catch {
            
        }
    }
    
    @objc func connectionEngineDidReceiveMeasure(_ notification: NSNotification) {
        if let measure = notification.userInfo?[ConnectionEngineUserInfoKeys.EOKMeasure] as? EOKMeasure {
            AppDelegate.sharedInstance.valoreZero = measure.rawPressure
            NotificationCenter.default.removeObserver(self, name: Notification.Name.ConnectionEngine.DidReceiveMeasureUpdate, object: nil)
            NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.DidUpdateZeroValue, object: nil)
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.DidConnectToPeripheral(_:)), name: Notification.Name.ConnectionEngine.DidConnectToPeripheral, object: nil)
        if let dpn = self.defaultPeripheralName {
            ConnectionEngine.shared.peripheralDefaultName = dpn
        }
        ConnectionEngine.zero = self.valoreZero ?? 0
        self.createDefaultTemplateIfNeeded()
        return true
    }
    
    @objc func DidConnectToPeripheral(_ notification: NSNotification) {
        if let peripheral = notification.userInfo?[ConnectionEngineUserInfoKeys.CBPeripheralKey] as? CBPeripheral {
            self.defaultPeripheralName = peripheral.name
            NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.connectionEngineDidReceiveMeasure(_:)), name: Notification.Name.ConnectionEngine.DidReceiveMeasureUpdate, object: nil)
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        do {
            let json = try String(contentsOf: url)
            let sessione = Session(context: self.persistentContainer.viewContext)
            try sessione.fillFromJSON(json: json)
            sessione.imported = "1"
            try self.persistentContainer.viewContext.save()
        } catch {
            print(error)
        }
        return true
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "EarOn")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK: - UINavigationControllerDelegate
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if let rnc = navigationController as? RecordingNavigationController,!rnc.appended {
            var bs = viewController.navigationItem.rightBarButtonItems ?? []
            let cv = UIImageView(image: UIImage(named: "rec"))
            cv.isUserInteractionEnabled = true
            cv.addGestureRecognizer(UITapGestureRecognizer(target: viewController, action: #selector(UIViewController.presentRecordigViewController(_:))))
            let b = UIBarButtonItem(customView: cv)
            bs.append(b)
            //bs.append(UIBarButtonItem(image: UIImage(named: "rec"), style: UIBarButtonItemStyle.done, target: viewController, action: #selector(UIViewController.presentRecordigViewController(_:))))
            //bs.append(UIBarButtonItem(barButtonSystemItem: .play, target: viewController, action: #selector(UIViewController.presentRecordigViewController(_:))))
            viewController.navigationItem.rightBarButtonItems = bs
            rnc.appended = true
        }
    }
    

}

