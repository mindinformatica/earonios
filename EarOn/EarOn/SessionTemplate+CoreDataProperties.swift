//
//  SessionTemplate+CoreDataProperties.swift
//  EarOn
//
//  Created by simonerocchi on 22/03/18.
//  Copyright © 2018 Uba Project. All rights reserved.
//
//

import Foundation
import CoreData


extension SessionTemplate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SessionTemplate> {
        return NSFetchRequest<SessionTemplate>(entityName: "SessionTemplate")
    }

    @NSManaged public var name: String?
    @NSManaged public var steps: [Int]?
    @NSManaged public var lastUseDate: NSDate?

}
