//
//  ImpostazioniTableViewController.swift
//  EarOn
//
//  Created by simonerocchi on 31/03/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import CoreBluetooth
import EarOnKit

class ImpostazioniTableViewController: UITableViewController,SettingViewController {
    var mode: String? {
        get {
            return AppDelegate.sharedInstance.mode
        }
        set(m) {
            AppDelegate.sharedInstance.mode = m
        }
    }
    
    var sessionTemplate: SessionTemplate? {
        get {
            return AppDelegate.sharedInstance.template
        }
        set(t) {
            AppDelegate.sharedInstance.template = t
        }
    }
    

    @IBOutlet weak var modelloLabel: UILabel!
    @IBOutlet weak var profiloLabel: UILabel!
    @IBOutlet weak var limitiLabel: UILabel!
    @IBOutlet weak var connectedPeripheralLabel: UILabel!
    @IBOutlet weak var batteryLevelLabel: UILabel!
    @IBOutlet weak var modelloTableViewCell: UITableViewCell!
    @IBOutlet weak var profiloImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let periferica = ConnectionEngine.shared.connectedPeripheral {
            self.connectedPeripheralLabel.text = periferica.name
            self.batteryLevelLabel.text = NSLocalizedString("Battery level not avalaible", comment: "Battery level not avalaible")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(ImpostazioniTableViewController.engineDidUpdateBatteryLevel(_:)), name: Notification.Name.ConnectionEngine.DidReceiveBatteryLevelUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ImpostazioniTableViewController.DidConnectToPeripheral(_:)), name: Notification.Name.ConnectionEngine.DidConnectToPeripheral, object: nil)
        NotificationCenter.default.addObserver(forName: Notification.Name.ConnectionEngine.ReadyToReadBatteryLevel, object: nil, queue: nil) { (notification) in
            ConnectionEngine.shared.readBatteryLevel()
        }
        ConnectionEngine.shared.scan()
        if AppDelegate.sharedInstance.limiteInferiore == nil
        && AppDelegate.sharedInstance.limiteSuperiore == nil
            && AppDelegate.sharedInstance.sogliaCompensazione == nil {
            self.limitiLabel.text = NSLocalizedString("No limit set", comment: "No limit set")
        } else {
            let lis = AppDelegate.sharedInstance.limiteInferiore == nil ? "" : "Inf: \(String(format: "%.1f",AppDelegate.sharedInstance.limiteInferiore!)) "
            let lss = AppDelegate.sharedInstance.limiteSuperiore == nil ? "" : "Sup: \(String(format: "%.1f",AppDelegate.sharedInstance.limiteSuperiore!)) "
            let cos = AppDelegate.sharedInstance.sogliaCompensazione == nil ? "" : "Comp: \(String(format: "%.1f",AppDelegate.sharedInstance.sogliaCompensazione!))"
            self.limitiLabel.text = "\(lis)\(lss)\(cos)"
        }
        self.profiloLabel.text = AppDelegate.sharedInstance.mode
        if let mode = AppDelegate.sharedInstance.mode {
            let imageName = "i_\(mode.replacingOccurrences(of: " ", with: "").lowercased())"
            self.profiloImageView.image = UIImage(named: imageName)
        } else {
            self.profiloImageView.image = nil
        }
        self.modelloLabel.text = AppDelegate.sharedInstance.template?.name
    }
    
    @IBAction func infoTapped(_ sender: Any) {
        UIApplication.shared.open(URL(string:"http://www.ubaproject.com")!)
    }
    override func viewWillDisappear(_ animated: Bool) {
        ConnectionEngine.shared.disconnect()
        ConnectionEngine.shared.stopScan()
        NotificationCenter.default.removeObserver(self)
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func engineDidUpdateBatteryLevel(_ notification: NSNotification) {
        if let batteryLevel = notification.userInfo?[ConnectionEngineUserInfoKeys.EOKBatteryLevel] {
            self.batteryLevelLabel.text = "\(NSLocalizedString("Battery level:", comment: "Battery level:")) \(batteryLevel)"
            ConnectionEngine.shared.disconnect()
            ConnectionEngine.shared.stopScan()
        }
    }
    
    @objc func DidConnectToPeripheral(_ notification: NSNotification) {
        if let peripheral = notification.userInfo?[ConnectionEngineUserInfoKeys.CBPeripheralKey] as? CBPeripheral {
            self.connectedPeripheralLabel.text = peripheral.name
            self.batteryLevelLabel.text = NSLocalizedString("Battery level not avalaible", comment: "Battery level not avalaible")
            ConnectionEngine.shared.readBatteryLevel()
        }
    }
    
    @IBAction func templateDidChange(_ segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func modeDidChange(_ segue: UIStoryboardSegue) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dc = segue.destination as? NoteLegaliViewController, segue.identifier == "noteLegaliSegue" {
            dc.navigationItem.rightBarButtonItems = nil
        }
    }
}
