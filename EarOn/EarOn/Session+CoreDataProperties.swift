//
//  Session+CoreDataProperties.swift
//  EarOn
//
//  Created by Simone Rocchi on 07/10/2019.
//  Copyright © 2019 Uba Project. All rights reserved.
//
//

import Foundation
import CoreData


extension Session {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Session> {
        return NSFetchRequest<Session>(entityName: "Session")
    }

    @NSManaged public var measures: [Any]?
    @NSManaged public var mode: String?
    @NSManaged public var peripheralName: String?
    @NSManaged public var startDate: Date?
    @NSManaged public var steps: [Int]?
    @NSManaged public var stopDate: Date?
    @NSManaged public var templateName: String?
    @NSManaged public var descr: String?
    @NSManaged public var imported: String?

}
