//
//  NoteLegaliViewController.swift
//  EarOn
//
//  Created by simonerocchi on 03/07/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit

class NoteLegaliViewController: UIViewController {

    @IBOutlet weak var accettoButton: UIBarButtonItem!
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let targetURL = Bundle.main.url(forResource: "Note Legali", withExtension: "pdf")! // This value is force-unwrapped for the sake of a compact example, do not do this in your code
        let request = URLRequest(url: targetURL)
        self.webView.loadRequest(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func accettoAction(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "notelegali")
        UserDefaults.standard.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
