//
//  RecordingViewController.swift
//  EarOn
//
//  Created by simonerocchi on 05/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import CoreData
import CoreBluetooth
import EarOnKit
import Charts
import AVFoundation

public protocol SettingViewController {
    var mode: String?{get set}
    var sessionTemplate: SessionTemplate? {get set}
    func templateDidChange(_ segue: UIStoryboardSegue)
    func modeDidChange(_ segue: UIStoryboardSegue)
}

class RecordingViewController: UIViewController, IAxisValueFormatter,UIScrollViewDelegate,SettingViewController,AVAudioPlayerDelegate {
    @IBOutlet weak var graphPageControl: UIPageControl!
    @IBOutlet weak var modeImageView: UIImageView!
    @IBOutlet weak var graphScrollView: UIScrollView!
    @IBOutlet weak var lapLabel: UILabel!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var countdownView: UIVisualEffectView!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var greyView: UIView!
    @IBOutlet var greyViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var durataLabel: UILabel!
    @IBOutlet weak var stepsStackView: StepsStackView!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var startButton: UIBarButtonItem!
    let startColor = UIColor(red: 126/255, green: 206/255, blue: 47/255, alpha: 1)
    var counter: Int = 0
    var startDate: Date?
    var measurs: [EOKMeasure] = [EOKMeasure]()
    var sessionTemplate: SessionTemplate? {
        didSet {
            self.showSessionTemplate()
        }
    }
    lazy var lastUsedLineColor: UIColor = StepsStackView.waitColor
    lazy var lastPressures: [Float] = []
    var pressureDataSetIndex: Int {
        get {
            return (self.lineChartView.data?.dataSets.count ?? 0) - 1
        }
    }
    lazy var session: Session = {
        return Session(context: AppDelegate.sharedInstance.persistentContainer.viewContext)
    }()
    var minPressure: Float? {
        get {
           return (self.session.mode ?? MouthfillRefill) == MouthfillRefill ? (AppDelegate.sharedInstance.limiteInferiore ?? 0) : nil
        }
    }
    lazy var maxPressure: Float? = AppDelegate.sharedInstance.limiteSuperiore
    lazy var compPressure: Float? = AppDelegate.sharedInstance.sogliaCompensazione
    lazy var maxDataSet: LineChartDataSet = {
        let ds = LineChartDataSet(values: nil, label: nil)
        ds.drawCirclesEnabled = false
        ds.drawCircleHoleEnabled = false
        ds.lineWidth = 1
        ds.colors = [UIColor.red]
        ds.highlightEnabled = false
        return ds
    }()
    lazy var minDataSet: LineChartDataSet = {
        let ds = LineChartDataSet(values: nil, label: nil)
        ds.drawCirclesEnabled = false
        ds.drawCircleHoleEnabled = false
        ds.lineWidth = 1
        ds.colors = [UIColor.red]
        ds.highlightEnabled = false
        return ds
    }()
    lazy var compDataSet: LineChartDataSet = {
        let ds = LineChartDataSet(values: nil, label: nil)
        ds.drawCirclesEnabled = false
        ds.drawCircleHoleEnabled = false
        ds.lineWidth = 1
        ds.colors = [UIColor.green]
        ds.highlightEnabled = false
        return ds
    }()
    var defaultTemplate: SessionTemplate? {
        return AppDelegate.sharedInstance.template
    }
    var mode: String? {
        get {
            return self.session.mode ?? "Free"
        }
        set(nm) {
            let m = nm?.lowercased() == "free" ? nil : nm
            self.session.mode = m
            self.modeImageView.image = m == nil ? nil : UIImage(named: m!)
            self.graphPageControl.numberOfPages = m == nil ? 1 : 2
            self.graphScrollView.isScrollEnabled = m != nil
            AppDelegate.sharedInstance.mode = nm
        }
    }
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        ai.color = UIColor.gray
        ai.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(ai)
        self.view.addConstraint(NSLayoutConstraint(item: ai, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: ai, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0))
        self.view.bringSubviewToFront(ai)
        ai.isHidden = true
        return ai
    }()
    
    var relevantPressureArrived: Bool = false
    
    var nearCompPressure: Bool = false
    
    /*lazy var geigerPlayer: AVAudioPlayer? = {
        if let url = Bundle.main.url(forResource: "KMCGeigerCounterTick", withExtension: "aiff") {
            do {
                let p = try AVAudioPlayer(contentsOf: url)
                p.delegate = self
                return p
            } catch {
                print(error)
            }
        }
        return nil
    }()*/
    
    //let maxGeigerInterval:TimeInterval = 1
    //var geigerTimer: Timer?
    
    //var cdPlayer: AVAudioPlayer?
    
    /*lazy var gameoverPlayer: AVAudioPlayer? = {
        if let url = Bundle.main.url(forResource: "gameover", withExtension: "flac") {
            do {
                let p = try AVAudioPlayer(contentsOf: url)
                p.numberOfLoops = -100
                return p
            } catch {
                print(error)
            }
        }
        return nil
    }()*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.isTranslucent = false
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
        }
        
        UIApplication.shared.isIdleTimerDisabled = true
        self.mode = AppDelegate.sharedInstance.mode
        self.sessionTemplate = self.defaultTemplate
        self.lineChartView.xAxis.drawGridLinesEnabled = false
        self.lineChartView.leftAxis.drawGridLinesEnabled = false
        self.lineChartView.rightAxis.drawGridLinesEnabled = false
        self.lineChartView.xAxis.labelCount = 8
        self.lineChartView.xAxis.valueFormatter = self
        self.lineChartView.legend.enabled = false
        self.lineChartView.noDataText = ""
        //let delta = 20.0
        //self.lineChartView.setVisibleYRange(minYRange: delta, maxYRange: delta, axis: .left)
        //self.lineChartView.setVisibleXRange(minXRange: 16000.0, maxXRange: 16000.0)
        //self.lineChartView.autoScaleMinMaxEnabled = true
        self.lineChartView.chartDescription?.text = ""
        self.lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        
        
        
        
        
        
        self.navigationController?.setToolbarHidden(false, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(RecordingViewController.connectionEngineDidConnect(_:)), name: Notification.Name.ConnectionEngine.DidUpdateZeroValue, object: nil)
        if self.sessionTemplate?.name == nil && self.sessionTemplate?.steps == nil {
            self.sessionTemplate = self.defaultTemplate
        }
        NotificationCenter.default.addObserver(forName: Notification.Name.TemplateNotificationNames.TemplateDidRemove, object: nil, queue: nil) { (notification) in
            if let templateName = notification.userInfo?[removedTemplateUserInfoKey] as? String, self.sessionTemplate?.name == templateName {
                self.sessionTemplate = self.defaultTemplate
            }
        }
        self.showSessionTemplate()
        ConnectionEngine.shared.scan()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.ConnectionEngine.DidConnectToPeripheral, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.ConnectionEngine.DidReceiveMeasureUpdate, object: nil)
        UIApplication.shared.isIdleTimerDisabled = false
        super.viewDidDisappear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sessionTemplateSegue" {
            if let dc = segue.destination as? TemplateViewController {
                dc.sessionTemplate = self.sessionTemplate
            }
        }
    }
    
    func showSessionTemplate() {
        if let st = self.sessionTemplate {self.durataLabel.text = st.formattedTotalLength
            self.stepsStackView.steps = st.steps!
            self.session.templateName = self.sessionTemplate?.name
            self.session.steps = self.sessionTemplate?.steps
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        ConnectionEngine.shared.disconnect()
        ConnectionEngine.shared.stopScan()
        AppDelegate.sharedInstance.persistentContainer.viewContext.delete(self.session)
        AppDelegate.sharedInstance.saveContext()
        //self.geigerTimer?.invalidate()
        //self.geigerPlayer?.stop()
        self.dismiss(animated: true, completion: nil)
    }
    
    func provideNewPressureDataSet(circles:Bool) -> LineChartDataSet! {
        let ds = LineChartDataSet(values: nil, label: nil)
        ds.drawCirclesEnabled = circles
        ds.drawCircleHoleEnabled = circles
        ds.lineWidth = 2
        ds.highlightEnabled = false
        return ds
    }
    
    // MARK: - ConnectionEngine management
    @objc func connectionEngineDidConnect(_ notification: NSNotification) {
        self.startButton.isEnabled = true
        self.navigationController?.toolbar.barTintColor = self.startColor
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    @objc func didReceiveMeasureUpdate(_ notification: NSNotification) {
        if var measure = notification.userInfo?[ConnectionEngineUserInfoKeys.EOKMeasure] as? EOKMeasure {
            var pressureToDisplay = measure.pressure
            if !self.relevantPressureArrived && measure.pressure >= 3 {
                self.relevantPressureArrived = true
            } else if !self.relevantPressureArrived {
                pressureToDisplay = 0
            }
            if let cp = self.compPressure {
                if !self.nearCompPressure && abs(pressureToDisplay - cp) < 3 {
                    self.nearCompPressure = true
                } else if self.nearCompPressure && cp - pressureToDisplay > 3 {
                    self.nearCompPressure = false
                }
            }
            measure.timestamp = Float(self.session.measures?.count ?? 0) * 125.0
            self.session.add(measure:measure)
            self.lastPressures.append(measure.pressure)
            if self.lastPressures.count > 80 {
                self.lastPressures.remove(at: 0)
            }
            let lineColor = self.session.lineColorFor(time: TimeInterval(Int(measure.timestamp / 1000)))
            if self.lineChartView?.data == nil {
                self.lineChartView.data = LineChartData(dataSets: [self.maxDataSet,self.minDataSet,self.compDataSet,self.provideNewPressureDataSet(circles: false)])
            }
            if self.pressureDataSetIndex > 0 {
                self.lineChartView.data?.addEntry(ChartDataEntry(x: Double(measure.timestamp), y: Double(pressureToDisplay)), dataSetIndex: self.pressureDataSetIndex)
            }
            //cambia colore oppure il una delle due soglie sta fra il valore attuale e il precedente
            if lineColor != self.lastUsedLineColor {
                let pds = self.provideNewPressureDataSet(circles: false)
                self.lineChartView.data?.addDataSet(pds)
                pds?.setColor(lineColor!)
                self.lineChartView.data?.addEntry(ChartDataEntry(x: Double(measure.timestamp), y: Double(measure.pressure)), dataSetIndex: self.pressureDataSetIndex)
                self.lastUsedLineColor = lineColor!
            }
            if let ma = self.maxPressure {
                self.lineChartView.data?.addEntry(ChartDataEntry(x: Double(measure.timestamp), y: Double(ma)), dataSetIndex: 0)
            }
            if let mi = self.minPressure {
                self.lineChartView.data?.addEntry(ChartDataEntry(x: Double(measure.timestamp), y: Double(mi)), dataSetIndex: 1)
            }
            if let co = self.compPressure {
                self.lineChartView.data?.addEntry(ChartDataEntry(x: Double(measure.timestamp), y: Double(co)), dataSetIndex: 2)
            }
            self.lineChartView.notifyDataSetChanged()
            if counter > 0 {
                self.lineChartView.setVisibleXRange(minXRange: 16000, maxXRange: 16000)
                self.lineChartView.moveViewToX(Double(measure.timestamp))
                //let delta = 20.0
                //self.lineChartView.setVisibleYRange(minYRange: delta, maxYRange: delta, axis: .left)
                //self.lineChartView.moveViewToY(/*(delta / 2) + minY*/ Double(measure.pressure), axis: .left)
                //self.lineChartView.moveViewTo(xValue: Double(measure.timestamp), yValue: Double(measure.pressure), axis: .left)
                //self.lineChartView.resetZoom()
                NSLayoutConstraint.deactivate([self.greyViewWidthConstraint])
                self.greyViewWidthConstraint = nil
                self.greyViewWidthConstraint = NSLayoutConstraint(item: self.greyView as Any, attribute: .width, relatedBy: .equal, toItem: self.stepsStackView, attribute: .width, multiplier: 1 - CGFloat(min(self.session.advancement ?? 1,1)), constant: 0)
                self.view.addConstraint(self.greyViewWidthConstraint)
                self.durataLabel.text = self.session.formatterdCurrentCountDown
                self.durataLabel.textColor = lineColor
                if self.session.currentTime >= self.session.totalLength {
                    self.startAction(self.startButton)
                }
            }

            if let c = self.compPressure {
                let d = c - measure.pressure
                print("DELTA: \(d)")
                DispatchQueue.main.async {
                    if d > 3 {
                        self.pressureLabel.textColor = UIColor.black
                        /*if (self.geigerPlayer?.isPlaying ?? false) {
                            self.geigerTimer?.invalidate()
                            self.geigerPlayer?.stop()
                        }
                        if self.gameoverPlayer?.isPlaying ?? false {
                            self.gameoverPlayer?.stop()
                        }*/
                    } else if d > -3 {
                        self.pressureLabel.textColor = UIColor.green
                        /*if !(self.geigerPlayer?.isPlaying ?? true) {
                            self.geigerTimer?.invalidate()
                            self.geigerPlayer?.play()
                        }
                        if self.gameoverPlayer?.isPlaying ?? false {
                            self.gameoverPlayer?.stop()
                        }*/
                    } else if measure.pressure < self.maxPressure ?? (c + 10) {
                        self.pressureLabel.textColor = UIColor.orange
                        /*if !(self.geigerPlayer?.isPlaying ?? true) {
                            self.geigerTimer?.invalidate()
                            self.geigerPlayer?.play()
                        }
                        if self.gameoverPlayer?.isPlaying ?? false {
                            self.gameoverPlayer?.stop()
                        }*/
                    } else {
                        self.pressureLabel.textColor = UIColor.red
                        /*if (self.geigerPlayer?.isPlaying ?? false) {
                            self.geigerTimer?.invalidate()
                            self.geigerPlayer?.stop()
                        }
                        if !(self.gameoverPlayer?.isPlaying ?? true) {
                            self.gameoverPlayer?.play()
                        }*/
                    }
                }
            }
            if counter % 8 == 0 && counter > 0 {
                self.pressureLabel.text = String(format: "%.1f hPa",measure.pressure)
                self.lapLabel.text = "\(self.session.currentLap)/\(self.session.totalLaps)"
                /*if self.session.steps?.count ?? 1 > 1 && self.session.currentCountDown < 4 && self.session.currentCountDown > 0 {
                    if let url = Bundle.main.url(forResource: "cd\(self.session.currentCountDown)", withExtension: "m4a") {
                        do {
                            self.cdPlayer = try AVAudioPlayer(contentsOf: url)
                            self.cdPlayer?.play()
                        } catch {
                            print(error)
                        }
                    }
                }*/
            }
            counter += 1
        }
    }
    @IBAction func startAction(_ sender: UIBarButtonItem) {
        if sender.tag == 3 {
            sender.tag = 5
            self.tabBarController?.tabBar.isHidden = true
            var i = 5
            self.countdownView.alpha = 0
            self.countdownView.isHidden = false
            self.countdownLabel.alpha = 0
            UIView.animate(withDuration: 1, animations: {
                self.countdownView.alpha = 1
            })
            UIView.animate(withDuration: 1, animations: {
                self.countdownLabel.alpha = 1
            })
            let _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                if i > 0 {
                    self.countdownLabel.text = "\(i)"
                    /*if let url = Bundle.main.url(forResource: "cd\(i)", withExtension: "m4a") {
                        do {
                            self.cdPlayer = try AVAudioPlayer(contentsOf: url)
                            self.cdPlayer?.play()
                        } catch {
                            print(error)
                        }
                    }*/
                } else {
                    timer.invalidate()
                    self.countdownView.isHidden = true
                    self.session.startDate = Date()
                    if let periferica = ConnectionEngine.shared.connectedPeripheral {
                        self.session.peripheralName = periferica.name
                    }
                    NotificationCenter.default.addObserver(self, selector: #selector(RecordingViewController.didReceiveMeasureUpdate(_:)), name: Notification.Name.ConnectionEngine.DidReceiveMeasureUpdate, object: nil)
                }
                i -= 1
            })
            self.navigationController?.toolbar.barTintColor = UIColor.red
            sender.title = NSLocalizedString("Stop", comment: "Stop")
        } else {
            sender.tag = 3
            //self.geigerPlayer?.stop()
            //self.geigerTimer?.invalidate()
            self.tabBarController?.tabBar.isHidden = false
            NotificationCenter.default.removeObserver(self, name: Notification.Name.ConnectionEngine.DidReceiveMeasureUpdate, object: nil)
            ConnectionEngine.shared.disconnect()
            ConnectionEngine.shared.stopScan()
            self.session.stopDate = Date()
            sender.title = NSLocalizedString("Start", comment: "Start")
            self.navigationController?.toolbar.barTintColor = self.startColor
            AppDelegate.sharedInstance.saveContext()
            if let nc = self.navigationController,
                let rc = self.storyboard?.instantiateViewController(withIdentifier: "recC") as? RecordingViewController,
                nc.topViewController == self {
                nc.viewControllers = [rc]
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func viewModeDidChange(_ sender: UISegmentedControl) {
        self.lineChartView.isHidden = sender.selectedSegmentIndex == 1
    }
    //MARK: - IAxisValueFormatter
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return "\(Int(value) / 1000)"
    }
    
    @IBAction func templateDidChange(_ segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func modeDidChange(_ segue: UIStoryboardSegue) {
        
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.graphPageControl.currentPage = scrollView.contentOffset.x == 0 ? 0 : 1
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.graphPageControl.currentPage = scrollView.contentOffset.x == 0 ? 0 : 1
    }
    /*func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if let fcp = self.compPressure, self.startButton.tag != 3 {
            let cp = Double(fcp)
            let minP = cp - 3.0
            let maxP = Double(self.maxPressure ?? Float(cp + 10.0))
            let lastPressure = min(maxP,Double(self.lastPressures.last ?? Float(cp)))
            let ti = maxGeigerInterval * (lastPressure - maxP) / (minP - maxP)
            print("INTERVALLO: \(ti) PRESSIONE:\(self.lastPressures.last ?? 666666666)")
            self.geigerTimer = Timer.scheduledTimer(withTimeInterval: ti, repeats: false) { (timer) in
                player.play()
            }
        }
        
    }*/
}
