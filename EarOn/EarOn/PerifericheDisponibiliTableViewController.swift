//
//  PerifericheDisponibiliTableViewController.swift
//  EarOn
//
//  Created by simonerocchi on 03/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import CoreBluetooth
import EarOnKit

class PerifericheDisponibiliTableViewController: UITableViewController {
        
    @IBOutlet weak var scanButton: UIBarButtonItem!
    
    var discoveredPeripherals: [CBPeripheral] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(PerifericheDisponibiliTableViewController.DidDiscoverPeripheral(_:)), name: Notification.Name.ConnectionEngine.DidDiscoverPeripheral, object: nil)
        NotificationCenter.default.addObserver(forName: Notification.Name.ConnectionEngine.DidScanForPeripherals, object: nil, queue: nil) { (notification) in
            self.scanButton.title = "Stop Scan"
        }
        NotificationCenter.default.addObserver(forName: Notification.Name.ConnectionEngine.DidStopScanningForPeripherals, object: nil, queue: nil) { (notification) in
            self.scanButton.title = "Scan"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ConnectionEngine.shared.scan()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        ConnectionEngine.shared.stopScan()
        super.viewDidDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.discoveredPeripherals.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PERIPHERAL_CELL", for: indexPath)

        let peripheral = self.discoveredPeripherals[indexPath.row]
        cell.textLabel?.text = peripheral.name
        if peripheral.name == AppDelegate.sharedInstance.defaultPeripheralName {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }

        return cell
    }
    
    @objc func DidDiscoverPeripheral(_ notification: NSNotification) {
        if let peripheral = notification.userInfo?[ConnectionEngineUserInfoKeys.CBPeripheralKey] as? CBPeripheral {
            if !self.discoveredPeripherals.contains(peripheral) {
                self.discoveredPeripherals.append(peripheral)
                let indexPath = IndexPath(row: self.discoveredPeripherals.count - 1, section: 0)
                self.tableView.insertRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let peripheral = self.discoveredPeripherals[indexPath.row]
        AppDelegate.sharedInstance.defaultPeripheralName = peripheral.name
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func scanButtonAction(_ sender: Any) {
        self.scan(!ConnectionEngine.shared.isScanning)
    }
    
    func scan(_ on: Bool) {
        if on {
            ConnectionEngine.shared.scan()
        } else {
            ConnectionEngine.shared.stopScan()
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
