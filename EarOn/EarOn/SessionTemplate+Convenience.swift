//
//  SessionTemplate+Convenience.swift
//  EarOn
//
//  Created by simonerocchi on 05/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import Foundation
import CoreData

public protocol SessionTemplateProtocol {
    var name: String? {get}
    var steps: [Int]? {get}
    var totalLength: Int {get}
    var formattedTotalLength: String {get}
}

extension SessionTemplate : SessionTemplateProtocol {
    public var formattedTotalLength: String {
        return AppDelegate.sharedInstance.defaultDateComponentsFormatter.string(from: TimeInterval(self.totalLength)) ?? ""
    }

    public var totalLength: Int {
        get {
            return self.steps?.reduce(0, +) ?? 0
        }
    }
    
    public convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.steps = aDecoder.decodeObject(forKey: "steps") as? [Int]
    }
}
