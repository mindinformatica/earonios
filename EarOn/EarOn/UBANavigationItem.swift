//
//  UBANavigationItem.swift
//  EarOn
//
//  Created by simonerocchi on 08/06/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit

class UBANavigationItem: UINavigationItem {
    @IBOutlet var titleLabel: UILabel?
    override var title: String? {
        get {
            return self.titleLabel?.text ?? ""
        }
        set(nt) {
            self.titleLabel?.text = nt
        }
    }
}
