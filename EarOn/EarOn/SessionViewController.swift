//
//  SessionTableViewController.swift
//  EarOn
//
//  Created by simonerocchi on 12/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import EarOnKit
import Charts

class SessionViewController: UIViewController, IAxisValueFormatter, UIDocumentInteractionControllerDelegate, ChartViewDelegate {
    @IBOutlet weak var modeOnLabel: UILabel!
    @IBOutlet var modeViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var visualEffectBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var visualEffectHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var modeView: UIView!
    @IBOutlet weak var stackScrollView: UIScrollView!
    @IBOutlet weak var modeLabel: UILabel!
    @IBOutlet weak var modeImageView: UIImageView!
    @IBOutlet weak var lapLabel: UILabel!
    @IBOutlet weak var minPressureLabel: UILabel!
    @IBOutlet weak var maxPressureLabel: UILabel!
    @IBOutlet weak var equalizationPressureLabel: UILabel!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var greyViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var templateLabel: UILabel!
    @IBOutlet weak var durataLabel: UILabel!
    @IBOutlet weak var stepsStackView: StepsStackView!
    @IBOutlet weak var pressureLabel: UILabel!
    lazy var lockModeViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.modeView as Any, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
    
    var sessionTemplate: SessionTemplateProtocol? {
        get {
            return self.session
        }
    }
    lazy var lastUsedLineColor: UIColor = StepsStackView.waitColor
    var pressureDataSetIndex: Int {
        get {
            return (self.lineChartView.data?.dataSets.count ?? 0) - 1
        }
    }
    var session: Session? {
        didSet {
            self.navigationItem.title = self.session?.formattedStartDate ?? ""
        }
    }
    lazy var minPressure: Float? = (self.session?.mode ?? MouthfillRefill) == MouthfillRefill ? (AppDelegate.sharedInstance.limiteInferiore ?? 0) : nil
    lazy var maxPressure: Float? = AppDelegate.sharedInstance.limiteSuperiore
    lazy var compPressure: Float? = AppDelegate.sharedInstance.sogliaCompensazione
    lazy var maxDataSet: LineChartDataSet = {
        let ds = LineChartDataSet(values: nil, label: nil)
        ds.drawCirclesEnabled = false
        ds.drawCircleHoleEnabled = false
        ds.lineWidth = 1
        ds.colors = [UIColor.red]
        ds.highlightEnabled = false
        return ds
    }()
    lazy var minDataSet: LineChartDataSet = {
        let ds = LineChartDataSet(values: nil, label: nil)
        ds.drawCirclesEnabled = false
        ds.drawCircleHoleEnabled = false
        ds.lineWidth = 1
        ds.colors = [UIColor.red]
        ds.highlightEnabled = false
        return ds
    }()
    lazy var compDataSet: LineChartDataSet = {
        let ds = LineChartDataSet(values: nil, label: nil)
        ds.drawCirclesEnabled = false
        ds.drawCircleHoleEnabled = false
        ds.lineWidth = 1
        ds.colors = [UIColor.green]
        ds.highlightEnabled = false
        return ds
    }()
    var dic: UIDocumentInteractionController?
    var modeViewOpen: Bool = false
    private var dateComponentsFormatter: DateComponentsFormatter {
        get {
            return AppDelegate.sharedInstance.defaultDateComponentsFormatter
        }
    }
    
    func provideNewPressureDataSet() -> LineChartDataSet! {
        let ds = LineChartDataSet(values: nil, label: nil)
        ds.drawCirclesEnabled = false
        ds.drawCircleHoleEnabled = false
        ds.lineWidth = 2
        return ds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lineChartView.delegate = self
        self.durataLabel.text = self.session?.duration
        self.showSessionTemplate()
        self.lineChartView.xAxis.drawGridLinesEnabled = false
        self.lineChartView.leftAxis.drawGridLinesEnabled = false
        self.lineChartView.rightAxis.drawGridLinesEnabled = false
        self.lineChartView.xAxis.labelCount = 8
        self.lineChartView.xAxis.valueFormatter = self
        self.lineChartView.legend.enabled = false
        self.lineChartView.chartDescription?.text = ""
        self.lineChartView.data = LineChartData(dataSets: [self.maxDataSet,self.minDataSet,self.compDataSet,self.provideNewPressureDataSet()])
        self.modeImageView.image = self.session?.mode == nil ? nil : UIImage(named: self.session!.mode!)
        if self.session?.mode == nil {
            self.modeLabel.textColor = UIColor.black
            self.modeOnLabel.textColor = UIColor.black
        }
        self.modeLabel.text = self.session?.mode ?? "Free"
        if let ep = AppDelegate.sharedInstance.sogliaCompensazione {
            self.equalizationPressureLabel.text = String(format: "%.1f hPa",ep)
        }
        if let maxp = self.maxPressure {
            self.maxPressureLabel.text = String(format: "%.1f hPa",maxp)
        }
        if let minp = self.minPressure {
            self.minPressureLabel.text = String(format: "%.1f hPa",minp)
        }
        self.navigationItem.prompt = self.session?.descr
        
        for measure in self.session?.eokMeasures ?? [] {
            let lineColor = self.session?.lineColorFor(time: TimeInterval(Int(measure.timestamp / 1000)))
            self.lineChartView.data?.addEntry(ChartDataEntry(x: Double(measure.timestamp), y: Double(measure.pressure)), dataSetIndex: self.pressureDataSetIndex)
            if lineColor != self.lastUsedLineColor {
                let pds = self.provideNewPressureDataSet()
                self.lineChartView.data?.addDataSet(pds)
                pds?.setColor(lineColor!)
                self.lineChartView.data?.addEntry(ChartDataEntry(x: Double(measure.timestamp), y: Double(measure.pressure)), dataSetIndex: self.pressureDataSetIndex)
                self.lastUsedLineColor = lineColor!
            }
            if let ma = self.maxPressure {
                self.lineChartView.data?.addEntry(ChartDataEntry(x: Double(measure.timestamp), y: Double(ma)), dataSetIndex: 0)
            }
            if let mi = self.minPressure {
                self.lineChartView.data?.addEntry(ChartDataEntry(x: Double(measure.timestamp), y: Double(mi)), dataSetIndex: 1)
            }
            if let co = self.compPressure {
                self.lineChartView.data?.addEntry(ChartDataEntry(x: Double(measure.timestamp), y: Double(co)), dataSetIndex: 2)
            }
        }
        self.lineChartView.notifyDataSetChanged()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.isTranslucent = false
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showSessionTemplate() {
        if let st = self.sessionTemplate {
            self.templateLabel.text = st.name
            self.durataLabel.text = st.formattedTotalLength
            self.stepsStackView.steps = st.steps!
        }
    }
    
    //MARK: - IAxisValueFormatter
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return "\(Int(value) / 1000)"
    }

    @IBAction func shareAction(_ sender: UIBarButtonItem) {
        let ac = UIAlertController(title: NSLocalizedString("Share your session", comment: "Share your session"), message: NSLocalizedString("You can share data or graphic", comment: "You can share data or graphic"), preferredStyle: .actionSheet)
        ac.addAction(UIAlertAction(title: NSLocalizedString("Data", comment: "Data"), style: .default, handler: { (a) in
            ac.dismiss(animated: true, completion: nil)
            if let csvString = self.session?.toJSON() {
                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                    let path = dir.appendingPathComponent("values.eqtool")
                    
                    //writing
                    do {
                        try csvString.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                        self.shareFileAt(url: path,sender: sender)
                    }
                    catch {/* error handling here */}
                }
            }
        }))
        ac.addAction(UIAlertAction(title: NSLocalizedString("Graphic (PNG)", comment: "Graphic (PNG)"), style: .default, handler: { (a) in
            ac.dismiss(animated: true, completion: nil)
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last {
                let path = dir.appendingPathComponent("grafico.png", isDirectory: false)
                var d: ObjCBool = ObjCBool(false)
                print(FileManager.default.fileExists(atPath: path.absoluteString, isDirectory: &d))
                print(d)
                do {
                    try FileManager.default.removeItem(at: path)
                } catch {
                    print(error)
                }
                if self.lineChartView.save(to: path.absoluteString, format: ChartViewBase.ImageFormat.png, compressionQuality: 1.0) {
                    self.shareFileAt(url: path,sender: sender)
                }
            }
            
        }))
        ac.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel, handler: { (a) in
            ac.dismiss(animated: true, completion: nil)
        }))
        ac.popoverPresentationController?.barButtonItem = sender
        self.present(ac, animated: true, completion: nil)
    }
    
    func shareFileAt(url: URL!,sender:UIBarButtonItem) {
        //condivido il file
        if self.dic != nil {
            self.dic!.dismissMenu(animated: true)
            self.dic = nil
        }
        self.dic = UIDocumentInteractionController(url: url)
        self.dic!.delegate = self
        self.dic!.presentOptionsMenu(from: sender, animated: true)
    }
    
    @IBAction func modeViewDidTapped(_ sender: Any) {
        print(self.modeView.frame)
        if self.session?.mode != nil {
            let chartHeight = self.lineChartView.frame.size.height
            self.visualEffectHeightConstraint.constant = self.visualEffectHeightConstraint.constant + (self.modeViewOpen ? -chartHeight : chartHeight)
            self.visualEffectBottomConstraint.constant = self.visualEffectBottomConstraint.constant + (self.modeViewOpen ? chartHeight : -chartHeight)
            //self.modeViewWidthConstraint.constant = self.modeViewOpen ? 0 : (self.view.frame.size.width / 3 * 2)
            self.view.removeConstraint(self.modeViewWidthConstraint)
            self.modeViewWidthConstraint = NSLayoutConstraint(item: self.modeView as Any, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: self.modeViewOpen ? (1 / 3) : 1, constant: 0)
            self.view.addConstraint(self.modeViewWidthConstraint)
            //        if self.modeViewOpen {
            //            self.view.removeConstraint(self.lockModeViewConstraint)
            //        } else {
            //            self.view.addConstraint(self.lockModeViewConstraint)
            //        }
            self.view.setNeedsUpdateConstraints()
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
                self.stackScrollView.contentOffset = self.modeView.frame.origin
                self.modeImageView.alpha = self.modeViewOpen ? 0 : 1
            }
            print(self.modeView.frame)
            //self.stackScrollView.scrollRectToVisible(self.modeView.frame, animated: true)
            self.stackScrollView.isScrollEnabled = self.modeViewOpen
            self.modeViewOpen = !self.modeViewOpen
            
        }
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print(chartView)
        print(entry)
        print(highlight)
        self.durataLabel.text = self.dateComponentsFormatter.string(from: entry.x / 1000)
        self.pressureLabel.text = String(format: "%.1f hPa",entry.y)
        self.lapLabel.text = "\(self.session!.lapFor(time: Int(entry.x / 1000)))/\(self.session!.totalLaps)"
    }
    
    @IBAction func visualEffectViewDidTapped(_ sender: Any) {
        self.lineChartView.highlightValue(nil, callDelegate: true)
    }
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        self.durataLabel.text = self.session?.duration
        self.pressureLabel.text = nil
        self.lapLabel.text = nil
    }
    @IBAction func descrEditAction(_ sender: Any) {
        //apro un popup con textbox
        let ac = UIAlertController(title: NSLocalizedString("Session description", comment: "Session description"), message: nil, preferredStyle: .alert)
        ac.addTextField { (textField) in
            //metto nella textbox un placeholder
            textField.placeholder = NSLocalizedString("type a description for your session", comment: "type a description for your session")
            //metto nella textbox l'eventuale valore di descr
            textField.text = self.session?.descr
        }
        //aggiungo un pulsante per annullare l'edit
        ac.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel, handler: nil))
        //aggiungo un pulsante per salvarlo
        ac.addAction(UIAlertAction(title: NSLocalizedString("Save", comment: "Save"), style: .default, handler: { (action) in
            //recupero la textbox
            if let textField = ac.textFields?.first {
                //recupero il testo
                let text = textField.text
                //lo assegno alla proprietà descr della sessione
                self.session?.descr = text
                //salvo
                AppDelegate.sharedInstance.saveContext()
                //aggiorno la label con la descrizione
                self.navigationItem.prompt = text
            }
        }))
        self.present(ac, animated: true, completion: nil)
    }
}
