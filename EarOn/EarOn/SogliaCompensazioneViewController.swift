//
//  SogliaCompensazioneViewController.swift
//  EarOn
//
//  Created by simonerocchi on 04/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import CoreBluetooth
import EarOnKit

class SogliaCompensazioneViewController: UIViewController {
    
    
    @IBOutlet weak var pressureLabel: UILabel!
    var counter: Int = 0
    var lastPressure: Float? {
        didSet {
            if self.counter % 8 == 0 {
                self.pressureLabel.text = String(format: "%.1f",self.lastPressure!)//"\(self.lastPressure!)"
            }
            self.counter += 1
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
        NotificationCenter.default.addObserver(self, selector: #selector(SogliaCompensazioneViewController.connectionEngineDidReceiveMeasure(_:)), name: Notification.Name.ConnectionEngine.DidReceiveMeasureUpdate, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ConnectionEngine.shared.scan()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        ConnectionEngine.shared.stopScan()
        ConnectionEngine.shared.disconnect()
        super.viewDidDisappear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func connectionEngineDidConnect(_ notification: NSNotification) {
        
    }
    
    @objc func connectionEngineDidReceiveMeasure(_ notification: NSNotification) {
        if let measure = notification.userInfo?[ConnectionEngineUserInfoKeys.EOKMeasure] as? EOKMeasure {
            self.lastPressure = measure.pressure
        }
    }

    @IBAction func doneButton(_ sender: Any) {
        AppDelegate.sharedInstance.limiteInferiore = self.lastPressure
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
