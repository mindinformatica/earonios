//
//  GameSettingsTableViewController.swift
//  EarOn
//
//  Created by simonerocchi on 19/06/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit

class GameSettingsTableViewController: UITableViewController {

    @IBOutlet weak var ballMassTextField: UITextField!
    @IBOutlet weak var conversionFactorTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ballMassTextField.text = "\(AppDelegate.sharedInstance.ballMass ?? 0)"
        self.conversionFactorTextField.text = "\(AppDelegate.sharedInstance.ballConversionFactor ?? 0)"
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    @IBAction func undoAction(_ sender: Any) {
        AppDelegate.sharedInstance.ballMass = nil
        self.ballMassTextField.text = "\(AppDelegate.sharedInstance.ballMass ?? 0)"
        self.conversionFactorTextField.text = "\(AppDelegate.sharedInstance.ballConversionFactor ?? 0)"
    }
    
    @IBAction func ballMassTextFieldEditingDidEnd(_ sender: Any) {
        if let tbm =  self.ballMassTextField.text, let fbm = Float(tbm) {
            let bm = CGFloat(fbm)
            AppDelegate.sharedInstance.ballMass = bm
        }
    }
    @IBAction func textFieldBlur(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
}
