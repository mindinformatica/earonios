//
//  TemplateViewController.swift
//  EarOn
//
//  Created by simonerocchi on 04/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import CoreData

let defaultTemplateName = "default"
let removedTemplateUserInfoKey = "removedTemplateUserInfoKey"

public extension Notification.Name {
    enum TemplateNotificationNames {
        public static let TemplateDidRemove = Notification.Name("TemplateDidRemove")
    }
}

class TemplateViewController: UIViewController, StepsStackViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    var sessionTemplate: SessionTemplate? {
        didSet {
            self.navigationItem.title = self.sessionTemplate?.name ?? ""
        }
    }
    @IBOutlet weak var stepPickerView: UIPickerView!
    @IBOutlet weak var stepStackView: StepsStackView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var removeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stepStackView.delegate = self
        if self.sessionTemplate == nil {
            self.sessionTemplate = SessionTemplate(context: AppDelegate.sharedInstance.persistentContainer.viewContext)
            self.sessionTemplate?.steps = [30]
            self.sessionTemplate?.name = NSLocalizedString("New template", comment: "New template")
        } else {
            self.navigationController?.title = self.sessionTemplate?.name
        }
        self.navigationController?.title = self.sessionTemplate?.name
        self.nameTextField.text = self.sessionTemplate?.name
        self.removeButton.isEnabled = (self.sessionTemplate?.steps?.count ?? 0) > 1
        self.stepStackView.steps = (self.sessionTemplate?.steps)!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setToolbarHidden(self.sessionTemplate?.name == defaultTemplateName, animated: true)
        self.nameTextField.isEnabled = self.sessionTemplate?.name != defaultTemplateName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nameDidChange(_ sender: UITextField) {
        self.sessionTemplate?.name = sender.text
    }
    //MARK: - StepsStackViewDelegate
    
    func stepValues(_ values: [Int], didChangeIn stepsStackView: StepsStackView) {
        self.sessionTemplate?.steps = values
        self.removeButton.isEnabled = (self.sessionTemplate?.steps?.count ?? 0) > 1
    }
    
    func selectedStepDidChange(selectedIndex: Int?, stepsStackView: StepsStackView) {
        self.stepPickerView.isHidden = selectedIndex == nil
        self.stepPickerView.selectRow((self.stepStackView.selectedStepLength ?? 1) - 1, inComponent: 0, animated: true)
    }
    
    //MARK: - UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return AppDelegate.sharedInstance.defaultDateComponentsFormatter.string(from: TimeInterval(row + 1)) ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.stepStackView.selectedStepLength = row + 1
    }
    
    //MARK: - UIPickerDataSource
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 180
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    @IBAction func addAction(_ sender: Any) {
        self.stepStackView.addStep(length: 30)
        self.stepStackView.selectedStepIndex = self.stepStackView.steps.count - 1
    }
    
    @IBAction func removeAction(_ sender: Any) {
        self.stepStackView.removeLastStep()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        
        AppDelegate.sharedInstance.persistentContainer.viewContext.delete(self.sessionTemplate!)
        //AppDelegate.sharedInstance.saveContext()
        if let templateName = self.sessionTemplate?.name {
            NotificationCenter.default.post(name: Notification.Name.TemplateNotificationNames.TemplateDidRemove, object: nil, userInfo: [removedTemplateUserInfoKey : templateName])
        }
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
