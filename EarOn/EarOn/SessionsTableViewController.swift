//
//  SessionsTableViewController.swift
//  EarOn
//
//  Created by simonerocchi on 06/04/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit
import CoreData

class SessionsTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    lazy var fetchedResultController: NSFetchedResultsController<Session> = {
        let moc = AppDelegate.sharedInstance.persistentContainer.viewContext
        let request: NSFetchRequest<Session> = Session.fetchRequest()
        let sd = [NSSortDescriptor(key: "imported", ascending: true),NSSortDescriptor(key: "startDate", ascending: false)]
        request.sortDescriptors = sd
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: "imported", cacheName: nil)
        frc.delegate = self
        return frc
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 66
        do {
            try self.fetchedResultController.performFetch()
        } catch {
            
        }
        if AppDelegate.sharedInstance.defaultPeripheralName == nil {
            self.tabBarController?.selectedIndex = 1
            if !UserDefaults.standard.bool(forKey: "notelegali"), let nc = self.tabBarController?.selectedViewController as? UINavigationController, let vc = nc.topViewController {
                vc.performSegue(withIdentifier: "noteLegaliFirstSegue", sender: nil)
            }
        } else {
            self.presentRecordigViewController(nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - FetchedResultControllerDelegate
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            self.tableView.reloadData()
//           self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .update:
            self.tableView.reloadData()
//            if let cell = self.tableView.cellForRow(at: newIndexPath!) as? SessionTableViewCell {
//                self.configure(cell: cell, session: anObject as! Session)
//            }
        case .delete:
            self.tableView.reloadData()
//            self.tableView.deleteRows(at: [indexPath!], with: .automatic)
        default: break
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.fetchedResultController.sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchedResultController.sections?[section].numberOfObjects ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SESSION_CELL", for: indexPath) as! SessionTableViewCell
        let session = self.fetchedResultController.object(at: indexPath)
        self.configure(cell: cell, session: session)
        
        return cell
    }
    
    func configure(cell:SessionTableViewCell, session:Session) {
        //cell.stepsStackView.steps = session.steps ?? []
        cell.startDateLabel.text = session.formattedStartDate ?? ""
        cell.durationLabel.text = session.duration ?? ""
        cell.modeLabel.text = NSLocalizedString(session.mode ?? "Free", comment: "")
        let imageName = "i_\((session.mode ?? "Free").replacingOccurrences(of: " ", with: "").lowercased())"
        cell.modeImageView.image = UIImage(named: imageName)
        cell.stepsStackView.steps = session.steps ?? []
        cell.descrLabel.text = session.descr ?? ""
        cell.setNeedsDisplay()
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            if let session = self.fetchedResultController.fetchedObjects?[indexPath.row] {
                AppDelegate.sharedInstance.persistentContainer.viewContext.delete(session)
                do {
                    try AppDelegate.sharedInstance.persistentContainer.viewContext.save()
                } catch let error {
                    print(error)
                }
            }
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.fetchedResultController.sections?[section].name == "0" ? nil : NSLocalizedString("Imported", comment: "Imported")
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "session_segue" {
            if let dc = segue.destination as? SessionViewController, let cell = sender as? UITableViewCell {
                if let indexPath = self.tableView.indexPath(for: cell) {
                    let session = self.fetchedResultController.object(at: indexPath)
                    dc.session = session
                }
            }
        }
    }
    

}
