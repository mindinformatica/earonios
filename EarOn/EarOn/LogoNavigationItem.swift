//
//  LogoNavigationItem.swift
//  EarOn
//
//  Created by simonerocchi on 26/05/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit

class LogoNavigationItem: UINavigationItem {
    private let logoImage: UIImageView = UIImageView(image: UIImage(named: "logoUBA"))
    private let actualTitleLabel: UILabel! = UILabel()
    override var title: String? {
        get {
            return actualTitleLabel.text
        }
        set(nt) {
            self.actualTitleLabel.text = nt
        }
    }
    override init(title: String) {
        super.init(title: title)
        self.putLogo()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.putLogo()
    }
    
    func putLogo() {
        if self.titleView == nil {
            let tv = UIView()
            tv.translatesAutoresizingMaskIntoConstraints = false
            self.titleView = tv
        }
        if let tv = self.titleView {
            tv.addSubview(self.logoImage)
            tv.addConstraint(NSLayoutConstraint(item: self.logoImage, attribute: .leading, relatedBy: .equal, toItem: tv, attribute: .leading, multiplier: 1, constant: 0))
            tv.addConstraint(NSLayoutConstraint(item: self.logoImage, attribute: .top, relatedBy: .equal, toItem: tv, attribute: .top, multiplier: 1, constant: 0))
            tv.addConstraint(NSLayoutConstraint(item: self.logoImage, attribute: .bottom, relatedBy: .equal, toItem: tv, attribute: .bottom, multiplier: 1, constant: 0))
            tv.addSubview(self.actualTitleLabel)
            tv.addConstraint(NSLayoutConstraint(item: self.actualTitleLabel as Any, attribute: .leading, relatedBy: .equal, toItem: self.logoImage, attribute: .trailing, multiplier: 1, constant: 0))
            tv.addConstraint(NSLayoutConstraint(item: self.actualTitleLabel as Any, attribute: .trailing, relatedBy: .equal, toItem: tv, attribute: .trailing, multiplier: 1, constant: 0))
        }
    }
}
