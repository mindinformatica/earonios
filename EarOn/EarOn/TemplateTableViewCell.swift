//
//  TemplateTableViewCell.swift
//  EarOn
//
//  Created by simonerocchi on 19/05/17.
//  Copyright © 2017 Uba Project. All rights reserved.
//

import UIKit

class TemplateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var stepStackView: StepsStackView!
    @IBOutlet weak var durataLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    var template: SessionTemplate? {
        didSet {
            self.nameLabel.text = template?.name
            self.durataLabel.text = AppDelegate.sharedInstance.defaultDateComponentsFormatter.string(from: TimeInterval((self.template?.totalLength)!))
            self.stepStackView.steps = self.template?.steps ?? []
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
