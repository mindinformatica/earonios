//
//  ConnectionEngine.swift
//  ConnectionEngine
//
//  Created by simonerocchi on 30/03/17.
//  Copyright © 2017 Mind Informatica srl. All rights reserved.
//

import UIKit
import CoreBluetooth

/**
 estensione che aggiunge al tipo Data il metodo hexEncodedString
 */
extension Data {
    /**
     restituisce il contenuto convertito in esadecimale
     */
    var hexEncodedString: String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}

public extension Notification.Name {
    enum ConnectionEngine {
        public static let DidDiscoverPeripheral = Notification.Name("DidDiscoverPeripheral")
        public static let DidReceiveStateUpdate = Notification.Name("DidReceiveStateUpdate")
        public static let DidReceiveMeasureUpdate = Notification.Name("DidReceiveMeasureUpdate")
        public static let DidReceiveBatteryLevelUpdate = Notification.Name("DidReceiveBatteryLevelUpdate")
        public static let WillConnectToPeripheral = Notification.Name("WillConnectToPeripheral")
        public static let DidConnectToPeripheral = Notification.Name("DidConnectToPeripheral")
        public static let ReadyToReadBatteryLevel = Notification.Name("ReadyToReadBatteryLevel")
        public static let DidScanForPeripherals = Notification.Name("DidScanForPeripherals")
        public static let DidStopScanningForPeripherals = Notification.Name("DidStopScanningForPeripherals")
        public static let DidUpdateZeroValue = Notification.Name("DidUpdateZeroValue")
    }
}

public enum ConnectionEngineUserInfoKeys: String {
    case CBCentralManagerKey = "CBCentralManagerKey"
    case CBCentralManagerStateKey = "CBCentralManagerStateKey"
    case CBPeripheralKey = "CBPeripheralKey"
    case EOKMeasure = "EOKMeasure"
    case EOKBatteryLevel = "EOKBatteryLevel"
}

public struct EOKMeasure {
    public var pressure: Float
    public var temperature: Float
    public var timestamp: Float
    public var rawPressure: Float
    
    public init(dictionary:[String:Float]) {
        self.pressure = dictionary["pressure"] ?? 0
        self.temperature = dictionary["temperature"] ?? 0
        self.timestamp = dictionary["timestamp"] ?? 0
        self.rawPressure = dictionary["rawPressure"] ?? 0
    }
    
    public init(dictionary:[String:NSNumber]) {
        self.pressure = Float(dictionary["pressure"] ?? 0)
        self.temperature = Float(dictionary["temperature"] ?? 0)
        self.timestamp = Float(dictionary["timestamp"] ?? 0)
        self.rawPressure = Float(dictionary["rawPressure"] ?? 0)
    }
    
    public init(pressure:Float,temperature:Float,timestamp:Float,rawPressure:Float) {
        self.pressure = pressure
        self.temperature = temperature
        self.timestamp = timestamp
        self.rawPressure = rawPressure
    }
    
    public func toDictionary() -> [String:Float] {
        return ["pressure":self.pressure,"temperature":self.temperature,"timestamp":self.timestamp,"rawPressure":self.rawPressure]
    }
}

/**
 struttura che definisce gli uuid di una caratteristica e del servizio a cui appartiene
 */
public struct CBCharacteristicIdentifier {
    var serviceUUID: CBUUID
    var characteristicUUID: CBUUID
    func conformsTo(_ characteristic: CBCharacteristic) -> Bool {
        return characteristic.uuid == self.characteristicUUID && characteristic.service.uuid == self.serviceUUID
    }
}
/** servizio che restituisce il livello di carica della batteria    */
public let EOKBatteryLevelCharacteristic = CBCharacteristicIdentifier(serviceUUID: CBUUID(string: "180F"), characteristicUUID: CBUUID(string: "2A19"))

/** servizio che restituisce le misure di temperatura e pressione   */
public let EOKMeasureCharacteristic = CBCharacteristicIdentifier(serviceUUID: CBUUID(string: "AFF70000-A3EF-4753-957C-9750A82F3AB4"), characteristicUUID: CBUUID(string: "AFF70001-A3EF-4753-957C-9750A82F3AB4"))

/**
    classe che gestisce lo scan, la connessione e il recupero delle informazioni
    da le periferiche di UBA
 */
@objc public class ConnectionEngine: NSObject,CBCentralManagerDelegate,CBPeripheralDelegate {
    
    public static let shared = ConnectionEngine()
    public static var zero: Float = 0
    lazy var cm: CBCentralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options: nil)
    var scanningRequested: Bool = false
    
    public var isScanning: Bool {
        get {
            return self.cm.isScanning
        }
    }
    
    /** la periferica gestita attualmente dal ConnectionEngine  */
    private var peripheral: CBPeripheral? {
        willSet(newp) {
            //se esiste già una periferica gestita
            if self.peripheral != nil {
                //ferma la connessione alla periferica
                self.cm.cancelPeripheralConnection(self.peripheral!)
            }
            //imposta la nuova periferica gestita
            self.peripheral = newp
        }
        didSet {
            //se la nuova periferica gestita non è null
            if self.peripheral != nil {
                //ferma la ricerca di periferiche
                self.cm.stopScan()
                //imposta il delegate a se stesso
                self.peripheral?.delegate = self
                //notifico che sto per connettermi
                NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.WillConnectToPeripheral, object: nil, userInfo: [ConnectionEngineUserInfoKeys.CBPeripheralKey : self.peripheral!])
                //si connette alla periferica
                self.cm.connect(self.peripheral!, options: [CBConnectPeripheralOptionNotifyOnNotificationKey : true])
            }
        }
    }
    
    public var peripheralDefaultName: String?
    
    /** restituisce in sola lettura la priferica gestita attualmente    */
    public var connectedPeripheral: CBPeripheral? {
        get {
            return self.peripheral
        }
    }
    
    private var batteryLevelCharacteristic: CBCharacteristic? {
        didSet {
            if self.batteryLevelCharacteristic != nil {
                NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.ReadyToReadBatteryLevel, object: nil)
            }
        }
    }
    
    private override init() {
        super.init()
    }
    
    public func scan() {
        if !self.cm.isScanning {
            if self.cm.state == .poweredOn {
                NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.DidScanForPeripherals, object: nil)
                self.cm.scanForPeripherals(withServices: nil, options: nil)
            } else {
                self.scanningRequested = true
            }
        }
    }
    
    public func stopScan() {
        self.cm.stopScan()
        NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.DidStopScanningForPeripherals, object: nil)
    }
    
    private func isConform(peripheral: CBPeripheral!) -> Bool {
        return (peripheral.name ?? "NO NAME").contains("UBA")
    }
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("centralManagerDidUpdateState \(central.state)")
        if central.state == .poweredOn && self.scanningRequested {
            central.scanForPeripherals(withServices: nil, options: nil)
            self.scanningRequested = false
            NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.DidScanForPeripherals, object: nil)
        }
        NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.DidReceiveStateUpdate, object: nil, userInfo: [ConnectionEngineUserInfoKeys.CBCentralManagerKey : central,ConnectionEngineUserInfoKeys.CBCentralManagerStateKey : central.state])
    }
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if self.isConform(peripheral: peripheral) {
            print("centralManager did discover \(String(describing: peripheral.name))")
            NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.DidDiscoverPeripheral, object: nil, userInfo: [ConnectionEngineUserInfoKeys.CBPeripheralKey : peripheral])
            if peripheral.name == self.peripheralDefaultName {
                self.peripheral = peripheral
            }
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("centralManaget didConnect \(peripheral.name!)")
        //notifico la connessione
        NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.DidConnectToPeripheral, object: nil, userInfo: [ConnectionEngineUserInfoKeys.CBPeripheralKey : peripheral])
        //cerco i servizi della periferica
        peripheral.discoverServices(nil)
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        print("centralManaget didDisconnect \(peripheral.name!), error \(String(describing: error))")
        self.batteryLevelCharacteristic = nil
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for s: CBService in peripheral.services! {
            peripheral.discoverCharacteristics(nil, for: s)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for c: CBCharacteristic in service.characteristics! {
            print("\(service.uuid.uuidString) -> \(c.uuid.uuidString)")
            peripheral.setNotifyValue(true, for: c)
            if EOKBatteryLevelCharacteristic.conformsTo(c) {
                self.batteryLevelCharacteristic = c
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        //se la caratteristica è quella delle misurazioni
        if EOKMeasureCharacteristic.conformsTo(characteristic) {
            if let data = characteristic.value {
                //recupero i tre valori
                let dataString = data.hexEncodedString
                let pString = self.bele(String(dataString[..<dataString.index(dataString.startIndex, offsetBy: 8)]))
                let tString = self.bele(String(dataString[dataString.index(dataString.startIndex, offsetBy: 8)..<dataString.index(dataString.startIndex, offsetBy: 16)]))
                let tsString = self.bele(String(dataString[dataString.index(dataString.startIndex, offsetBy: 16)...]))
                
                let rawPressure = Float(Int(pString!,radix:16)!) / 100
                let pressure = rawPressure - ConnectionEngine.zero
                let temperature = Float(Int(tString!,radix:16)!) / 100
                let timeStamp = Float(Int(tsString!,radix:16)!)
                //invio al delegate i valori ricevuti
                let measure = EOKMeasure(pressure: pressure, temperature: temperature, timestamp: timeStamp, rawPressure:rawPressure)
                NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.DidReceiveMeasureUpdate, object: nil, userInfo: [ConnectionEngineUserInfoKeys.EOKMeasure : measure])
            }
        //se invece è quella del livello della batteria
        } else if EOKBatteryLevelCharacteristic.conformsTo(characteristic) {
            if let data = characteristic.value {
                //recupero il valore
                let dataString = data.hexEncodedString
                let batteryLevel = Double(Int(dataString,radix:16)!)
                //lo invio al delegate
                NotificationCenter.default.post(name: Notification.Name.ConnectionEngine.DidReceiveBatteryLevelUpdate, object: nil, userInfo: [ConnectionEngineUserInfoKeys.EOKBatteryLevel : batteryLevel])
            }
        }
    }
    
    /**
     restituisce l'esadecimale al contrario
    */
    func bele(_ origin:String!) -> String! {
        let l = origin.count
        var i = 0;
        var result:String = "";
        while(i + 2 <= l) {
            result = origin[origin.index(origin.startIndex, offsetBy: i)..<origin.index(origin.startIndex, offsetBy: i + 2)] + result
            i += 2
        }
        return result
    }
    
    /**
     inizia la connessione alla periferica passata
    */
    public func connectTo(peripheral: CBPeripheral!) {
        //imposto la nuova periferica per l'engine
        self.peripheral = peripheral
    }
    
    public func disconnect() {
        self.peripheral = nil
    }
    
    public func readBatteryLevel() {
        if let c = batteryLevelCharacteristic {
            self.peripheral?.readValue(for: c)
        }
    }
}
