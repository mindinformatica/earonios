//
//  EarOnKit.h
//  EarOnKit
//
//  Created by simonerocchi on 30/03/17.
//  Copyright © 2017 Mind Informatica srl. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for EarOnKit.
FOUNDATION_EXPORT double EarOnKitVersionNumber;

//! Project version string for EarOnKit.
FOUNDATION_EXPORT const unsigned char EarOnKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <EarOnKit/PublicHeader.h>
