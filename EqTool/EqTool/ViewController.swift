//
//  ViewController.swift
//  UBA
//
//  Created by simonerocchi on 16/05/16.
//  Copyright © 2016 Mind Informatica. All rights reserved.
//

import UIKit
import CoreBluetooth

extension Data {
    var hexEncodedString: String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}

class ViewController: UIViewController,CBCentralManagerDelegate,CBPeripheralDelegate,UITextFieldDelegate,UIDocumentInteractionControllerDelegate {
    @IBOutlet weak var connettiBtn: UIButton!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var nomeTextBox: UITextField!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var stopBtn: UIButton!
    @IBOutlet weak var statoLabel: UILabel!
    @IBOutlet weak var deltaLabel: UILabel!
    lazy var eventQueue: DispatchQueue = DispatchQueue(label: "com.mind-informatica.UBA", attributes: [])
    lazy var chars: [CBUUID:AnyObject] = [CBUUID:AnyObject]()
    lazy var descrs: [CBUUID:AnyObject] = [CBUUID:AnyObject]()
    var timer: Timer?
    var cm: CBCentralManager?
    var peripheral: CBPeripheral? {
        didSet {
            DispatchQueue.main.async {
                self.startBtn.isEnabled = self.peripheral != nil
                if self.peripheral != nil {
                    self.peripheral?.delegate = self
                    self.statoLabel.text = "Connesso a \(self.peripheral!.name!)"
                } else {
                    self.statoLabel.text = ""
                }
            }
        }
    }
    var lastPressure: Double?
    var data: [[Any]]?
    var dic: UIDocumentInteractionController?
    lazy var counter: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()  
//        self.eventQueue.setTarget(queue: DispatchQueue.main)
        self.statoLabel.text = ""
        self.nomeTextBox.text = UserDefaults.standard.string(forKey: "PERIFERICA") ?? ""
        self.connettiBtn.isEnabled = (self.nomeTextBox.text?.characters.count ?? 0) > 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if peripheral.name?.trimmingCharacters(in: .whitespacesAndNewlines) == self.nomeTextBox.text ?? "" {
            self.peripheral = peripheral
            central.stopScan()
            self.cm!.connect(self.peripheral!, options: [CBConnectPeripheralOptionNotifyOnNotificationKey : true])
        }
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            central.scanForPeripherals(withServices: nil, options: nil)
        case .poweredOff:
            if self.peripheral != nil {
                central.cancelPeripheralConnection(self.peripheral!)
            }
            central.stopScan()
        default: break
        }
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
    
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.delegate = self
        
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.discover(_:)), userInfo: nil, repeats: false)
        
        
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        if self.peripheral == peripheral {
            self.peripheral = nil
        }
    }

    @objc func discover(_ timer: Timer) {
        self.peripheral?.discoverServices(nil)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for s: CBService in peripheral.services! {
            peripheral.discoverCharacteristics(nil, for: s)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for c: CBCharacteristic in service.characteristics! {
            peripheral.setNotifyValue(true, for: c)
            peripheral.readValue(for: c)
            peripheral.discoverDescriptors(for: c)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        for d: CBDescriptor in characteristic.descriptors! {
            peripheral.readValue(for: d)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
        if self.descrs[descriptor.uuid] == nil && descriptor.value != nil {
            self.descrs[descriptor.uuid] = descriptor.value as AnyObject?
        } else if !(self.descrs[descriptor.uuid] as? String ?? "BOH").isEqual(descriptor.value ?? "BOH") {
            self.descrs[descriptor.uuid] = descriptor.value as AnyObject?
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if self.chars[characteristic.uuid] == nil && characteristic.value != nil {
            self.chars[characteristic.uuid] = characteristic.value as AnyObject?
        } else if !(self.chars[characteristic.uuid] as? String ?? "BOH").isEqual(characteristic.value ?? "BOH") {
            self.chars[characteristic.uuid] = characteristic.value as AnyObject?
        }
        if characteristic.uuid.uuidString == "AFF70001-A3EF-4753-957C-9750A82F3AB4" {
            if let data = characteristic.value {
                let dataString = data.hexEncodedString
                let pString = self.bele(dataString.substring(to: dataString.index(dataString.startIndex, offsetBy: 8)))
                let tString = self.bele(dataString.substring(with: dataString.index(dataString.startIndex, offsetBy: 8)..<dataString.index(dataString.startIndex, offsetBy: 16)))
                let tsString = self.bele(dataString.substring(from: dataString.index(dataString.startIndex, offsetBy: 16)))
                
                let pressure = Double(Int(pString!,radix:16)!) / 100
                let temperature = Double(Int(tString!,radix:16)!) / 100
                let timeStamp = Double(Int(tsString!,radix:16)!)// / 100
                if self.lastPressure == nil {
                    self.lastPressure = pressure
                }
                let delta = ((pressure - self.lastPressure!) * 100).rounded() / 100
                print("\(Date())  DATA:\(dataString) PRESSIONE:\(pressure) TEMPERATURA:\(temperature) TIMESTAMP:\(timeStamp)")
                if self.data != nil {
                    self.data?.append([timeStamp,pressure,temperature,delta,dataString])
                }
                if self.counter % 8 == 0 {
                    DispatchQueue.main.async {
                        self.temperatureLabel.text = "\(temperature)"
                        self.pressureLabel.text = "\(pressure)"
                        self.deltaLabel.text = "\(delta)"
                    }
                }
                self.counter += 1
            }
        }
    }
    
    @IBAction func nomeTextBoxEditingChanged(_ sender: Any) {
        self.connettiBtn.isEnabled = (self.nomeTextBox.text?.characters.count ?? 0) > 0
    }
    
    @IBAction func startBtnAction(_ sender: Any) {
        self.data = [[Any]]()
        self.startBtn.isEnabled = false
        self.stopBtn.isEnabled = true
    }
    
    @IBAction func stopBtnAction(_ sender: Any) {
        //se data non è null
        if self.data != nil {
            //compongo la stringa
            var csvString = "TIMESTAMP;TEMPERATURA;PRESSIONE;DELTA PRESSIONE;DATA\n"
            for row in self.data! {
                csvString += "\(row[0]);\(row[1]);\(row[2]);\(row[3]);\(row[4])\n"
            }
            //scrivo il file da condividere
            let file = "values.csv"
            
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                
                let path = dir.appendingPathComponent(file)
                
                //writing
                do {
                    try csvString.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                    //metto data a null
                    self.data = nil
                    //condivido il file
                    if self.dic != nil {
                        self.dic!.dismissMenu(animated: true)
                        self.dic = nil
                    }
                    self.dic = UIDocumentInteractionController(url: path)
                    self.dic!.delegate = self
                    self.dic!.presentOptionsMenu(from: self.stopBtn.frame, in: self.view, animated: true)
                }
                catch {/* error handling here */}
            }
            //abilito start
            self.startBtn.isEnabled = true
            //disabilito stop
            self.stopBtn.isEnabled = false
        }
    }
    
    func bele(_ origin:String!) -> String! {
        let l = origin.characters.count
        var i = 0;
        var result:String = "";
        while(i + 2 <= l) {
            result = origin.substring(with: origin.index(origin.startIndex, offsetBy: i)..<origin.index(origin.startIndex, offsetBy: i + 2)) + result
            i += 2
        }
        return result
    }
    
    @IBAction func connettiAction(_ sender: AnyObject) {
        
        if self.cm == nil {
            self.connettiBtn.setTitle("Disconnetti", for: .normal)
            //let centralQueue = DispatchQueue(label: "central_queue", attributes: [])
            self.cm = CBCentralManager(delegate: self, queue: /*centralQueue*/DispatchQueue.main, options: nil)
            self.nomeTextBox.resignFirstResponder()
            UserDefaults.standard.set(self.nomeTextBox.text, forKey: "PERIFERICA")
            UserDefaults.standard.synchronize()
        } else {
            self.connettiBtn.setTitle("Connetti", for: .normal)
            if self.cm?.isScanning ?? false {
                self.cm?.stopScan()
            }
            if self.peripheral != nil {
                self.cm?.cancelPeripheralConnection(self.peripheral!)
            }
            self.cm = nil
        }
    }
}

